# build oci containers

## some theory

In a container we don't need a kernel. As a matter of fact we could put a single application
and it's dependencies in a container. This is called "micro architecture".

`poky` provides an `image-container.bbclass`[1] which helps us to build a root file system suitable
for inclusion in a container image. The `meta-virtualization`[2] layer provides 
an `image-oci.bbclass`[3] which allows us to build an oci container image.
As opposed to the previous approach[4] we don't need `docker` anymore to create an oci image, but e.g. a `docker`
runtime is able to execute oci images. As opposed to the previous approach[4] we can also build different 
architectures than just `x86-64` with the oci container approach. The oci images can be integrated into 
the target root file system or uploaded to a container registry e.g. via `skopeo`[5]. 

[1] https://git.yoctoproject.org/cgit/cgit.cgi/poky/tree/meta/classes/image-container.bbclass

[2] http://git.yoctoproject.org/cgit/cgit.cgi/meta-virtualization/tree/

[3] https://git.yoctoproject.org/cgit/cgit.cgi/meta-virtualization/tree/classes/image-oci.bbclass

[4] https://gitlab.com/meta-layers/meta-virtualization-tests/-/blob/master/01.00-build-docker/docker-build.md

[5] https://github.com/RobertBerger/skopeo-container

## rootfs built with `poky` + `meta-virtualization`

Let's try to use `poky`, `meta-virtualization` plus a custom layer to create an x86-64 (qemu) based root oci image.

## checkout layers

In my setup I use `/workdir/sources` as the place where I'll keep the `meta-layers` and `/workdir/build` for 
the actual build.

### get poky
on the host:
```
cd /workdir/sources
git clone git://git.yoctoproject.org/poky poky-master
```

### get meta-virtualization
on the host:
```
cd /workdir/sources
git clone git://git.yoctoproject.org/meta-virtualization meta-virtualization-master
```

### get meta-openembedded
on the host:
```
cd /workdir/sources
git clone git://git.openembedded.org/meta-openembedded meta-openembedded-master
```

### get meta-container-examples
on the host:
```
cd /workdir/sources
git clone https://gitlab.com/meta-layers/meta-container-ex-compact.git meta-container-ex-compact-oci -b oci
```

### get meta-container-ex-compact
on the host:
```
cd /workdir/sources
git clone https://gitlab.com/meta-layers/meta-container-ex-compact.git meta-container-ex-compact-oci -b oci
```

## checkout support scripts

### get manifests/support scripts
on the host:
```
cd /workdir/sources
git clone https://github.com/RobertBerger/manifests
```
## set some symlinks
on the host:
```
cd /workdir
ln -sf sources/manifests/resy-cooker.sh resy-cooker.sh
ln -sf sources/manifests/resy-poky-container.sh resy-poky-container.sh
ln -sf sources/manifests/killall_bitbake.sh killall_bitbake.sh
ln -sf sources/manifests/oci-copy-to-docker.sh oci-copy-to-docker.sh
```

## What to build

Let's say we wanted to build an oci image for `x86-64`.
In the rootfs we want `lighttpd` and `busybox`.

## `meta-container-ex-compact`
I created `meta-container-ex-compact` from various other layers of mine in order to cook up relatively 
simple examples here.

If we inspect `meta-container-ex-compact` on the `oci` branch this is what we'll see:

on the host:
```
tree /workdir/sources/meta-container-ex-compact-oci
```

you should see something like that:
```
meta-container-ex-compact-oci/
├── classes
│   └── image-manifestinfo.bbclass
├── conf
│   ├── distro
│   │   ├── include
│   │   │   └── common-all-distro.inc
│   │   └── resy-container.conf
│   ├── layer.conf
│   └── machine
│       └── container-x86-64.conf
├── COPYING.MIT
├── dynamic-layers
│   └── virtualization-layer
│       └── recipes-core
│           └── images
│               ├── app-container-image-lighttpd-oci.bb
│               └── app-container-image-oci.bb
├── README
├── recipes-core
│   └── images
│       ├── app-container-image-lighttpd-common.inc
│       └── common-img.inc
├── scripts
│   ├── config.sh
│   ├── patch-modified.sh
│   ├── push-upstream.sh
│   ├── remote-get-modified-files.sh
│   ├── remote-get-untracked-files.sh
│   ├── remote-scp-modified-files.sh
│   ├── remote-scp-untracked-files.sh
│   ├── remove-modified-and-untracked.sh
│   ├── scp-modified-files.sh
│   ├── scp-untracked-files.sh
│   └── untar-untracked.sh
└── template-container-x86-64-ex-compact-oci
    ├── 01-local.conf.glibc.plus-lic-info
    ├── 02-local.conf.glibc.less-metadata
    ├── 03-local.conf.musl.less-metadata
    ├── bblayers.conf.sample
    ├── conf-notes.txt
    ├── local.conf.sample
    └── site.conf

13 directories, 29 files
```

Let's have a look at these directories and files and see what they doing and which can be ignored.
### classes (dir)

```
/workdir/sources/meta-container-ex-compact
├── classes
│   └── image-manifestinfo.bbclass
```

This contains `image-manifestinfo.bbclass`[6] which is actually completely irrelevant for containers.

[6] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/classes/image-manifestinfo.bbclass
 
The only thing it does is to write image-maniufest information to `/etc/image-manifest`.
It's there because my default files include it, but you can igore it.

### conf (dir)

```
├── conf
│   ├── distro
│   │   ├── include
│   │   │   └── common-all-distro.inc
│   │   └── resy-container.conf
│   ├── layer.conf
│   └── machine
│       └── container-x86-64.conf
```

#### conf/distro

```
├── conf
│   ├── distro
│   │   ├── include
│   │   │   └── common-all-distro.inc
│   │   └── resy-container.conf
```

`resy-container.conf`[7] defines the `resy-container` distro, which is a wrapper around the `poky` distro
and removes a few things which are not needed for the container root file system.
It includes `common-all-distro.inc` [8] which adds a few variables to the buildinfo. 
I find this information convenient in my containers/targets but it is non essential for containers.

[7] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/conf/distro/resy-container.conf

[8] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/conf/distro/include/common-all-distro.inc

#### conf/machine

```
├── conf
│   └── machine
│       └── container-x86-64.conf
```

`container-x86-64.conf` [9] is the machine configuration. For simplicity reasons it is based on `qemux86-64.conf` 
which comes with `openembedded-core`/`poky` and replaces the default kernel provider with `linux-dummy`.

[9] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/conf/machine/container-x86-64.conf

### default files

```
/workdir/sources/meta-container-ex-compact
├── COPYING.MIT
├── README
```

I left them as they were composed from the `bitbake-layers` tool - sorry.

### image recipe

```
├── dynamic-layers
│   └── virtualization-layer
│       └── recipes-core
│           └── images
│               ├── app-container-image-lighttpd-oci.bb
│               └── app-container-image-oci.bb
```

```
├── recipes-core
│   └── images
│       ├── app-container-image-lighttpd-common.inc
│       └── common-img.inc
```

This is how I strctured it:

`app-container-image-lighttpd-oci.bb` [10] includes `app-container-image-oci.bb` [11], which is the oci equivalent
of `app-container-image.bb` [12]. `app-container-image-lighttpd-oci.bb` [10] also includes `app-container-image-lighttpd-common.inc` [13]. Remember? 
This is the common file which is shared with the previous approach [14]. 

`app-container-image-oci.bb` [11] includes `common-img.inc` [15].

Let's have a look at those files:

`app-container-image-lighttpd-oci.bb` [10] is the top level file and the one which will buidl our root file system. As you can see it just includes a couple of files.

`app-container-image-oci.bb` [11] is my "standard" include template for oci container images. You could just use it as it is.

`app-container-image-lighttpd-common.inc` [13] contains the packages which we would like to install into our root file system. Note that it is struictured like that and called `common` because it is common e.g. between this example here
and docker only images.

`common-img.inc` [15] just does something I find convenient, but it is non essential for containes.

[10] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/dynamic-layers/virtualization-layer/recipes-core/images/app-container-image-lighttpd-oci.bb

[11] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/dynamic-layers/virtualization-layer/recipes-core/images/app-container-image-oci.bb

[12] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/recipes-core/images/app-container-image.bb

[13] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/recipes-core/images/app-container-image-lighttpd-common.inc

[14] https://gitlab.com/meta-layers/meta-virtualization-tests/-/blob/master/01.00-build-docker/docker-build.md

[15] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/recipes-core/images/common-img.inc

### custom standard scripts

```
├── scripts
│   ├── config.sh
│   ├── patch-modified.sh
│   ├── push-upstream.sh
│   ├── remote-get-modified-files.sh
│   ├── remote-get-untracked-files.sh
│   ├── remote-scp-modified-files.sh
│   ├── remote-scp-untracked-files.sh
│   ├── remove-modified-and-untracked.sh
│   ├── scp-modified-files.sh
│   ├── scp-untracked-files.sh
│   └── untar-untracked.sh
```

You can totally ignore them. I use them internally for my workflow.

### template

```
└── template-container-x86-64-ex-compact-oci
    ├── 01-local.conf.glibc.plus-lic-info
    ├── 02-local.conf.glibc.less-metadata
    ├── 03-local.conf.musl.less-metadata
    ├── bblayers.conf.sample
    ├── conf-notes.txt
    ├── local.conf.sample
    └── site.conf
```

This is my custom template configuration directory. The underlying mechanism is desribed here [16]

[16] https://docs.yoctoproject.org/dev-manual/common-tasks.html#creating-a-custom-template-configuration-directory

We checked out `/workdir/sources/manifests` above and created some symlinks, which are used, among other things, 
to call the template configuration.

#### bblayers.conf

`bblayers.conf.conf` [17] is the template for `bblayers.conf`. As you can see only the layers in `poky`, `meta-virtualization`, dependencies of `meta-virtualization` and `meta-container-ex-compact` are used.

[17] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/template-container-x86-64-ex-compact-master/bblayers.conf.sample

#### conf-notes.txt

`conf-notes.txt` [18] just displays possible build targets like `app-container-image-lighttpd-oci`.

[18] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/template-container-x86-64-ex-compact-master/conf-notes.txt

#### local.conf

`local.conf.sample` [19] is out top level config file. Especially important are those variables:

[19] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/template-container-x86-64-ex-compact-master/local.conf.sample

this is our custom machine config:

```
MACHINE ??= "container-x86-64"
```

this is our custom distro:
```
DISTRO ?= "resy-container"
```

### site.conf

`site.conf` [20] is customized for my local setup. You might give it a try with it, ignore it, or adjust it to your setup. Please note that my wrapper scripts will automatically copy it over once.

[20] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/oci/template-container-x86-64-ex-compact-master/site.conf

## build container and setup

The assumption is, that you have a docker engine installed on your PC for this to work.

Please note that for this to work out of the box (without you hacking my scripts) you need to have the directly
structure as mentioned above. 

I build my stuff preperably in a build container. This is how I set it up:

on the host:
```
cd /workdir
./resy-poky-container.sh container-x86-64-ex-compact-oci

```

This should download a docker container and bring you to a shell inside the container, 
where the yocto build environment is already setup for you.

You should see somerhing along those lines:

```
+ docker pull reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18
2021-05-07-master-local-gcc-9-gui-icecc-ub18: Pulling from reliableembeddedsystems/poky-container
Digest: sha256:3f83f549071bfb1bdf1609b0bb977d4ba494ac5cdaa262e641a09a7d1827cb4a
Status: Image is up to date for reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18
docker.io/reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18
+ set +x
 -- interactive mode + MACHINE --
+ export BUILDDIR=/workdir/build/container-x86-64-ex-compact-oci
+ BUILDDIR=/workdir/build/container-x86-64-ex-compact-oci
+ /workdir/killall_bitbake.sh
student    338 32765  0 22:10 pts/1    00:00:00 /bin/bash /workdir/killall_bitbake.sh
student    340   338  0 22:10 pts/1    00:00:00 grep bitbake
student    338 32765  0 22:10 pts/1    00:00:00 /bin/bash /workdir/killall_bitbake.sh
student    345   338  0 22:10 pts/1    00:00:00 grep bitbake
rm -f /workdir/build/container-x86-64-ex-compact-oci/hashserve.sock
+ docker run --name poky_container --rm -i -t --add-host mirror:192.168.42.1 --net=host --env BUILD_ALL=no -v /home/student/projects:/projects -v /opt:/nfs -v /workdir:/workdir -v /workdir:/workdir reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18 /bin/bash -c 'source /workdir/resy-cooker.sh container-x86-64-ex-compact-oci && /bin/bash' --workdir=/workdir
/workdir ~
MACHINE or MACHINE-sw-variant: container-x86-64-ex-compact-oci
initial SITE_CONF=../../sources/meta-resy/template-common/site.conf.sample
TEMPLATECONF: ../meta-container-ex-compact-oci/template-container-x86-64-ex-compact-oci
source ../sources/poky-master/oe-init-build-env container-x86-64-ex-compact-oci
You had no conf/local.conf file. This configuration file has therefore been
created for you with some default values. You may wish to edit it to, for
example, select a different MACHINE (target hardware). See conf/local.conf
for more information as common configuration options are commented.

You had no conf/bblayers.conf file. This configuration file has therefore been
created for you with some default values. To add additional metadata layers
into your configuration please add entries to conf/bblayers.conf.

The Yocto Project has extensive documentation about OE including a reference
manual which can be found at:
    http://yoctoproject.org/documentation

For more information about OpenEmbedded see their website:
    http://www.openembedded.org/


### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are: 
    app-container-image-lighttpd-oci

You can also run generated qemu images with a command like 'runqemu qemux86'
conf
├── bblayers.conf   
├── local.conf
├── site.conf
└── templateconf.cfg

0 directories, 4 files
~
--> $#: 1
pokyuser    62    22  0 20:10 pts/0    00:00:00 /bin/bash /workdir/killall_bitbake.sh
pokyuser    64    62  0 20:10 pts/0    00:00:00 grep bitbake
pokyuser    62    22  0 20:10 pts/0    00:00:00 /bin/bash /workdir/killall_bitbake.sh
pokyuser    69    62  0 20:10 pts/0    00:00:00 grep bitbake
rm -f /workdir/build/container-x86-64-ex-compact-oci/hashserve.sock
to ./resy-poky-container.sh:
 -- non-interactive mode --
 add the image you want to build to the command line ./resy-poky-container.sh <MACHINE> <image>
 -- interactive mode --
   enter it with - ./resy-poky-container.sh <no param>
   bitbake <image>  
   source /workdir/resy-cooker.sh <MACHINE>
pokyuser@e450-13:~$ 
```
You see above that `pokyuser` is your current user, which indicates we are in a docker container:

In the container:
```
bitbake app-container-image-lighttpd-oci
```
And you should see something along those lines:
```
NOTE: Started PRServer with DBfile: /workdir/build/container-x86-64-ex-compact-oci/cache/prserv.sqlite3, IP: 127.0.0.1, PORT: 35993, PID: 89
WARNING: You have included the meta-virtualization layer, but 'virtualization' has not been enabled in your DISTRO_FEATURES. Some bbappend files may not take effect. See the meta-virtualization README for details on enabling virtualization support.
Loading cache: 100% |                                                                                                                                                         | ETA:  --:--:--
Loaded 0 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:03:19
Parsing of 2428 .bb files complete (0 cached, 2428 parsed). 3718 targets, 357 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies

Build Configuration:
BB_VERSION           = "1.50.0"
BUILD_SYS            = "x86_64-linux"
NATIVELSBSTRING      = "ubuntu-18.04"
TARGET_SYS           = "x86_64-resy-linux"
MACHINE              = "container-x86-64"
DISTRO               = "resy-container"
DISTRO_VERSION       = "3.3+snapshot-c21419b1fee3098298a77efdcca322fa665dea54"
TUNE_FEATURES        = "m64 core2"
TARGET_FPU           = ""
meta                 
meta-poky            
meta-yocto-bsp       = "master:c21419b1fee3098298a77efdcca322fa665dea54"
meta-container-ex-compact-oci = "oci:68d5d063b1de1185fbae13dc42d7c142cdc778e3"
meta-filesystems     
meta-python          
meta-networking      
meta-oe              = "master:dfbea6291077e50c0126c8d093794e50febf420d"
meta-virtualization-master = "master:a4f08e110e4b87a07b94c5c450fff25029a22ddb"

Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:01
Sstate summary: Wanted 583 Local 581 Network 0 Missed 2 Current 0 (99% match, 0% complete)
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 1737 tasks of which 1582 didn't need to be rerun and all succeeded.
NOTE: Writing buildhistory
NOTE: Writing buildhistory took: 1 seconds

Summary: There was 1 WARNING message shown.
```

Now we have an oci image which we can e.g. upload with `skopeo` to a docker registry.

On the host:
```
ls -lah /workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64/app-container-image-lighttpd-oci-container-x86-64-*.rootfs-oci-latest-x86_64-linux.oci-image.tar
```

You should see something like this:
```
-rw-r--r-- 2 pokyuser pokyuser 3.9M May  2 20:16 /workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64/app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci-latest-x86_64-linux.oci-image.tar
```

## Docker registry

So far we didn't use any third party tools. I guess we also could create with this a approach a container
which contains `skopeo` but at the time I started playing with this stuff it did not work as expected.

In order to work with the docker registry you need to have an account e.g. on `hub.docker.com` [21]

Log in and create a new project e.g. `container-ex-compact-oci-lighttpd-x86-64`.

[21] https://hub.docker.com/

## Use skopeo to push to registry

### Get container repo and support scripts

On the host:
```
cd /workdir
mkdir oci-container-x86-64 && cd oci-container-x86-64
git clone git://github.com/RobertBerger/skopeo-container -b skopeo-v1.1.0
cd /workdir/oci-container-x86-64/skopeo-container/non-local_scripts
```

### Start skopeo container

On the host:
```
cd /workdir/oci-container-x86-64/skopeo-container/non-local_scripts
./docker_run-ash.sh reliableembeddedsystems/skopeo-container:v1.1.0
```

You should see something along those lines:
```
+++ docker ps -a -q --filter ancestor=reliableembeddedsystems/skopeo-container:v1.1.0
++ ID_TO_KILL=
++ docker ps -a
CONTAINER ID        IMAGE                                                                                 COMMAND                  CREATED             STATUS              PORTS                  NAMES
b454f8e4bb25        reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18   "/usr/bin/dumb-init …"   11 hours ago        Up 11 hours                                poky_container
ee99589104d5        reslocal/app-container-lighttpd-x86-64                                                "/bin/sh -c '/etc/in…"   14 hours ago        Up 14 hours         0.0.0.0:8079->80/tcp   hungry_pasteur
++ docker stop
"docker stop" requires at least 1 argument.
See 'docker stop --help'.

Usage:  docker stop [OPTIONS] CONTAINER [CONTAINER...]

Stop one or more running containers
++ docker rm -f
"docker rm" requires at least 1 argument.
See 'docker rm --help'.

Usage:  docker rm [OPTIONS] CONTAINER [CONTAINER...]

Remove one or more containers
++ docker ps -a
CONTAINER ID        IMAGE                                                                                 COMMAND                  CREATED             STATUS              PORTS                  NAMES
b454f8e4bb25        reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18   "/usr/bin/dumb-init …"   11 hours ago        Up 11 hours                                poky_container
ee99589104d5        reslocal/app-container-lighttpd-x86-64                                                "/bin/sh -c '/etc/in…"   14 hours ago        Up 14 hours         0.0.0.0:8079->80/tcp   hungry_pasteur
++ docker pull reliableembeddedsystems/skopeo-container:v1.1.0
v1.1.0: Pulling from reliableembeddedsystems/skopeo-container
df20fa9351a1: Pull complete 
b236b635a301: Pull complete 
451818906f1f: Pull complete 
0062451eca1f: Pull complete 
Digest: sha256:1f4bf28c2ca5fbfd99c4592b2583388c20f0efab2d28ba1df907fe770f7005ee
Status: Downloaded newer image for reliableembeddedsystems/skopeo-container:v1.1.0
docker.io/reliableembeddedsystems/skopeo-container:v1.1.0
+++ docker run -v /workdir/oci-container-x86-64/skopeo-container/non-local_scripts/../../../:/workdir -t -i -d reliableembeddedsystems/skopeo-container:v1.1.0 /bin/sh -l
++ ID=146612801fdf4e0e48d25e0d746ef6b17df2dcd9a9cae323026b71f7a8211701
++ docker attach 146612801fdf4e0e48d25e0d746ef6b17df2dcd9a9cae323026b71f7a8211701
146612801fdf:/# 
```

### Push to registry

From within the container:
```
cd /workdir
./oci-copy-to-docker.sh
```

You will need to add your docker registry usename and password as instructed
```
export DOCKER_USER="reliableembeddedsystems"
export DOCKER_PW="some-super-secure-pw"
```

```
./oci-copy-to-docker.sh app-container-image-lighttpd-oci-container-x86-64 container-ex-compact-oci-lighttpd-x86-64 latest-x86-64 container-x86-64-ex-compact-oci conta
iner-x86-64 tmp
```

You should see meting like that:
```
image name: app-container-image-lighttpd-oci-container-x86-64
container name: container-ex-compact-oci-lighttpd-x86-64
tag: latest-x86-64
image container name (only used for deploy dir): container-x86-64-ex-compact-oci
machine (only used for deploy dir): container-x86-64
if there is a repository on docker hub called:
docker://docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64
deploy dir: /workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64/
press <ENTER>
```
```
+ cd /workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64/
+ set +x
+ pwd
+ HERE_AGAIN=/workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64
+ rm -rf unzip
+ mkdir unzip
+ cd unzip
+ ls -la ../app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci-latest-x86_64-linux.oci-image.tar
-rw-r--r--    2 1000     1000       4085760 May  2 20:16 ../app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci-latest-x86_64-linux.oci-image.tar
+ tar xvf ../app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci-latest-x86_64-linux.oci-image.tar
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/b15ac67540e63e9c122a446f6d915a6f4ff8dc5353d4dfbe3213cb074dcf15e3
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/5f2ec673a6da41266caa7f1d93de2436b1f169687a2583ee15c3fcee84e617a0
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/index.json
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/oci-layout
+ grep+  rootfs-oci
ls
+ OCI=app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci
+ set +x
FATA[0001] Error parsing image name "docker://reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64": Error reading manifest latest-x86-64 in docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64: manifest unknown: manifest unknown
--- aa.json
+++ bb.json
@@ -1 +1 @@
-[ "sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c" ]
+
looks like something is different now - might be only meta-data
press <ENTER> to go on 
```

Don't worry about the error above. I compare checksums between what's on docker hub and what I have locally
and it just says that they are different, hence we should upload.

```
+ cd /workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64
+ rm -rf unzip
+ mkdir unzip
+ cd unzip
+ ls -la ../app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci-latest-x86_64-linux.oci-image.tar
-rw-r--r--    2 1000     1000       4085760 May  2 20:16 ../app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci-latest-x86_64-linux.oci-image.tar
+ tar xvf ../app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci-latest-x86_64-linux.oci-image.tar
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/b15ac67540e63e9c122a446f6d915a6f4ff8dc5353d4dfbe3213cb074dcf15e3
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/blobs/sha256/5f2ec673a6da41266caa7f1d93de2436b1f169687a2583ee15c3fcee84e617a0
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/index.json
app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci/oci-layout
+ ls
+ OCI=app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci
+ date '+%Y-%m-%d_%H-%M-%S'
+ TIMESTAMP=2021-05-03_07-23-34
+ '[' -f container-ex-compact-oci-lighttpd-x86-64-latest-x86-64.skopeo.tar ]
+ skopeo copy oci:app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci docker-archive:container-ex-compact-oci-lighttpd-x86-64-latest-x86-64.skopeo.tar.docker-archive:reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64
Getting image source signatures
Copying blob 009202223ef9 done
Copying config 5f2ec673a6 done
Writing manifest to image destination
Storing signatures
+ skopeo copy oci:app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci:latest docker-archive:container-ex-compact-oci-lighttpd-x86-64-latest-x86-64-2021-05-03_07-23-34.skopeo.tar:reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64
Getting image source signatures
Copying blob 009202223ef9 done
Copying config 5f2ec673a6 done
Writing manifest to image destination
Storing signatures
+ skopeo --debug copy -f v2s2 --dest-creds reliableembeddedsystems:some-super-secure-pw oci:app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci:latest docker://reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64
DEBU[0000] Returning credentials from DockerAuthConfig
DEBU[0000] Using registries.d directory /etc/containers/registries.d for sigstore configuration
DEBU[0000]  No signature storage configuration found for docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64
DEBU[0000] Looking for TLS certificates and private keys in /etc/docker/certs.d/docker.io
DEBU[0000] Loading registries configuration "/etc/containers/registries.conf"
DEBU[0000] Using blob info cache at /var/lib/containers/cache/blob-info-cache-v1.boltdb
DEBU[0000] IsRunningImageAllowed for image oci:/workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64/unzip/app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci   
DEBU[0000]  Using default policy section
DEBU[0000]  Requirement 0: allowed
DEBU[0000] Overall: allowed
Getting image source signatures
DEBU[0000] Manifest has MIME type application/vnd.oci.image.manifest.v1+json, ordered candidate list [application/vnd.docker.distribution.manifest.v2+json]
DEBU[0000] Checking /v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c
DEBU[0000] GET https://registry-1.docker.io/v2/
DEBU[0000] Ping https://registry-1.docker.io/v2/ status 401
DEBU[0000] GET https://auth.docker.io/token?account=reliableembeddedsystems&scope=repository%3Areliableembeddedsystems%2Fcontainer-ex-compact-oci-lighttpd-x86-64%3Apull%2Cpush&service=registry.docker.io
DEBU[0001] HEAD https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c 
DEBU[0001] ... not present
DEBU[0001] Detected compression format gzip
DEBU[0001] Using original blob without modification
DEBU[0001] Checking /v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c
DEBU[0001] HEAD https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c 
Copying blob 009202223ef9 [--------------------------------------] 0.0b / 3.9MiB
DEBU[0002] ... not present
DEBU[0002] Uploading /v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/uploads/
Copying blob 009202223ef9 [--------------------------------------] 0.0b / 3.9MiB
DEBU[0003] PATCH https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/uploads/4e81f8f9-0802-46a9-b396-16119b2c708a?_state=Tp27zF1L9asE69X4Z0Stl5mcyXP6vMjtv0CALvBamA97Ik5hbWUiOiJyZWxpYWJsZWVtYmVkZGVkc3lzdGVtcy9jb250YWluZXItZXgtY29tcGFjdC1vY2ktbGlnaHR0cGQteDg2LTY0IiwiVVVJRCI6IjRlODFmOGY5LTA4MDItNDZhOS1iMzk2LTE2MTE5YjJjNzA4YSIsIk9mCopying blob 009202223ef9 done
DEBU[0006] PUT https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/uploads/4e81f8f9-0802-46a9-b396-16119b2c708a?_state=T2QH0sftKXN3FFhxetWqvm5S_IpjZjrGtmw_Q4jBwdx7Ik5hbWUiOiJyZWxpYWJsZWVtYmVkZGVkc3lzdGVtcy9jb250YWluZXItZXgtY29tcGFjdC1vY2ktbGlnaHR0cGQteDg2LTY0IiwiVVVJRCI6IjRlODFmOGY5LTA4MDItNDZhOS1iMzk2LTE2MTE5YjJjNzA4YSIsIk9mZnCopying blob 009202223ef9 done
DEBU[0007] Upload of layer sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c complete
DEBU[0007] No compression detected
DEBU[0007] Using original blob without modification
DEBU[0007] Checking /v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:5f2ec673a6da41266caa7f1d93de2436b1f169687a2583ee15c3fcee84e617a0
DEBU[0007] HEAD https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:5f2ec673a6da41266caa7f1d93de2436b1f169687a2583ee15c3fcee84e617a0 
Copying config 5f2ec673a6 [--------------------------------------] 0.0b / 510.0b
DEBU[0008] ... not present
DEBU[0008] Uploading /v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/uploads/
Copying config 5f2ec673a6 [--------------------------------------] 0.0b / 510.0b
DEBU[0009] PATCH https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/uploads/1bfb2a80-8908-44dd-8a03-8eb78f2390b9?_state=S0zYDIgNAYvhIaEAlawSeida21sxsa_hunpt0BfAe_p7Ik5hbWUiOiJyZWxpYWJsZWVtYmVkZGVkc3lzdGVtcy9jb250YWluZXItZXgtY29tcGFjdC1vY2ktbGlnaHR0cGQteDg2LTY0IiwiVVVJRCI6IjFiZmIyYTgwLTg5MDgtNDRkZC04YTAzLThlYjc4ZjIzOTBiOSIsIk9mCopying config 5f2ec673a6 done
DEBU[0009] PUT https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/uploads/1bfb2a80-8908-44dd-8a03-8eb78f2390b9?_state=6w8NkcP_ukjRSgKUm5QP46Tl38F0K5SRLZftSqC17mB7Ik5hbWUiOiJyZWxpYWJsZWVtYmVkZGVkc3lzdGVtcy9jb250YWluZXItZXgtY29tcGFjdC1vY2ktbGlnaHR0cGQteDg2LTY0IiwiVVVJRCI6IjFiZmIyYTgwLTg5MDgtNDRkZC04YTAzLThlYjc4ZjIzOTBiOSIsIk9mZnCopying config 5f2ec673a6 done
DEBU[0010] Upload of layer sha256:5f2ec673a6da41266caa7f1d93de2436b1f169687a2583ee15c3fcee84e617a0 complete
Writing manifest to image destination
DEBU[0011] PUT https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/manifests/latest-x86-64
Storing signatures
+ skopeo --debug copy -f v2s2 --dest-creds reliableembeddedsystems:some-super-secure-pw oci:app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci:latest docker://reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64-2021-05-03_07-23-34
DEBU[0000] Returning credentials from DockerAuthConfig
DEBU[0000] Using registries.d directory /etc/containers/registries.d for sigstore configuration
DEBU[0000]  No signature storage configuration found for docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64-2021-05-03_07-23-34
DEBU[0000] Looking for TLS certificates and private keys in /etc/docker/certs.d/docker.io
DEBU[0000] Loading registries configuration "/etc/containers/registries.conf"
DEBU[0000] Using blob info cache at /var/lib/containers/cache/blob-info-cache-v1.boltdb
DEBU[0000] IsRunningImageAllowed for image oci:/workdir/build/container-x86-64-ex-compact-oci/tmp/deploy/images/container-x86-64/unzip/app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci   
DEBU[0000]  Using default policy section
DEBU[0000]  Requirement 0: allowed
DEBU[0000] Overall: allowed
Getting image source signatures
DEBU[0000] Manifest has MIME type application/vnd.oci.image.manifest.v1+json, ordered candidate list [application/vnd.docker.distribution.manifest.v2+json]
DEBU[0000] Checking /v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c
DEBU[0000] GET https://registry-1.docker.io/v2/
DEBU[0000] Ping https://registry-1.docker.io/v2/ status 401
DEBU[0000] GET https://auth.docker.io/token?account=reliableembeddedsystems&scope=repository%3Areliableembeddedsystems%2Fcontainer-ex-compact-oci-lighttpd-x86-64%3Apull%2Cpush&service=registry.docker.io
DEBU[0001] HEAD https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c 
DEBU[0002] ... already exists
DEBU[0002] Skipping blob sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c (already present):
Copying blob 009202223ef9 [--------------------------------------] 0.0b / 0.0b
DEBU[0002] No compression detected
DEBU[0002] Using original blob without modification
DEBU[0002] Checking /v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:5f2ec673a6da41266caa7f1d93de2436b1f169687a2583ee15c3fcee84e617a0
DEBU[0002] HEAD https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/blobs/sha256:5f2ec673a6da41266caa7f1d93de2436b1f169687a2583ee15c3fcee84e617a0 
Copying config 5f2ec673a6 [--------------------------------------] 0.0b / 510.0b
DEBU[0003] ... already exists
Writing manifest to image destination
DEBU[0003] PUT https://registry-1.docker.io/v2/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64/manifests/latest-x86-64-2021-05-03_07-23-34
Storing signatures
+ jq .Layers
+ skopeo inspect oci:app-container-image-lighttpd-oci-container-x86-64-20210502201501.rootfs-oci
[
  "sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c"
]
+ skopeo inspect docker://reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64
+ jq .Layers
[
  "sha256:009202223ef9b9e2aa396fd491650748d610f22f8348fa641b87dfff6038147c"
]
+ set +x
try:
docker pull docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64
docker pull docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64
docker pull docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64-2021-05-03_07-23-34
```

## Docker

On Docker hub you should now see something like that:

![alt text](img/docker-hub-1.png "Docker Hub")


## Run it with a docker engine:

On the host:
```
docker pull docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64
```

You should see something like that:
```
latest-x86-64: Pulling from reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64
009202223ef9: Pull complete 
Digest: sha256:c90b387d001f899d16f9b3e3b8420ef11b4c596e17cff876b081eba3d3df6475
Status: Downloaded newer image for reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64
docker.io/reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64
```

On the host:
```
docker run -p 8078:80 --entrypoint=/bin/sh reliableembeddedsystems/container-ex-compact-oci-lighttpd-x86-64:latest-x86-64 -c '/etc/init.d/lighttpd restart && tail -f /var/log/access.log'
```
You should see something like this:
```
Restarting Lighttpd Web Server: no /usr/sbin/lighttpd found; none killed
lighttpd.
```

In a browser go to `<ip-address>:8078` and you should see:

![alt text](img/it-works-1.png "It works")

In the docker container you should see:
```
Restarting Lighttpd Web Server: no /usr/sbin/lighttpd found; none killed
lighttpd.
192.168.42.52 192.168.42.113:8078 - [03/May/2021:08:45:54 +0000] "GET / HTTP/1.1" 200 45 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
192.168.42.52 192.168.42.113:8078 - [03/May/2021:08:45:55 +0000] "GET /favicon.ico HTTP/1.1" 404 341 "http://192.168.42.113:8078/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
192.168.42.52 192.168.42.113:8078 - [03/May/2021:08:48:12 +0000] "GET / HTTP/1.1" 200 45 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
192.168.42.52 192.168.42.113:8078 - [03/May/2021:08:48:13 +0000] "GET /favicon.ico HTTP/1.1" 404 341 "http://192.168.42.113:8078/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
```