# podman related tests (rootful)

## clean up

```
multi-v7-ml login: root
```
```
root@multi-v7-ml:~# podman ps -a
```
could show something like this:
```
CONTAINER ID  IMAGE                              COMMAND   CREATED       STATUS                   PORTS                                           NAMES
adfe4c55e353  k8s.gcr.io/pause:3.2                         40 hours ago  Exited (0) 20 hours ago  0.0.0.0:3000->3000/tcp, 0.0.0.0:8086->8086/tcp  04dafa1c083d-infra
b3da1367db1f  docker.io/library/influxdb:1.8.4   influxd   40 hours ago  Exited (0) 20 hours ago  0.0.0.0:3000->3000/tcp, 0.0.0.0:8086->8086/tcp  influxdb
03ca697202dc  docker.io/grafana/grafana:7.4.0              40 hours ago  Created                  0.0.0.0:3000->3000/tcp, 0.0.0.0:8086->8086/tcp  grafana
681c50cc5fd0  docker.io/library/telegraf:1.17.2  telegraf  39 hours ago  Exited (2) 20 hours ago                                                  telegraf
```

stop all containers:
```
root@multi-v7-ml:~# podman stop $(podman ps -a -q)
```
you might see something like this:
```
681c50cc5fd01c11d94a4494589298372d339c8ca482bd5af91604e823f03744
adfe4c55e353ed5e104d46e54303daba3698e12c868cc7d6b1a427b5044fbd71
b3da1367db1fb0278e8f800b426c693ed927829015262486e80d97fdb640ad60
Error: can only stop created or running containers. 03ca697202dcf088ca57eccc5b493836aa7206bb3eb731e18f48cee92d1fa61d is in state configured: container state improper
```

remove the running containers:
```
root@multi-v7-ml:~# podman rm -f $(podman ps --no-trunc -a -q)
```
you might see something like this:
```
b3da1367db1fb0278e8f800b426c693ed927829015262486e80d97fdb640ad60
03ca697202dcf088ca57eccc5b493836aa7206bb3eb731e18f48cee92d1fa61d
681c50cc5fd01c11d94a4494589298372d339c8ca482bd5af91604e823f03744
Error: container adfe4c55e353ed5e104d46e54303daba3698e12c868cc7d6b1a427b5044fbd71 is the infra container of pod 04dafa1c083dca7ab422732bc667c1bf66b98ed6693001ee2a01d24aaae8918d and cannot be removed without removing the pod
```
confirm that they are gone:
```
root@multi-v7-ml:~# podman ps -a
CONTAINER ID  IMAGE                 COMMAND  CREATED       STATUS                   PORTS                                           NAMES
adfe4c55e353  k8s.gcr.io/pause:3.2           40 hours ago  Exited (0) 20 hours ago  0.0.0.0:3000->3000/tcp, 0.0.0.0:8086->8086/tcp  04dafa1c083d-infra
root@multi-v7-ml:~# 
```

Please note that the images should be still here
```
root@multi-v7-ml:~# podman images
```
you might see something like this:
```
REPOSITORY                  TAG     IMAGE ID      CREATED        SIZE
docker.io/library/influxdb  1.8.4   b981467ac4ae  3 days ago     270 MB
docker.io/library/telegraf  1.17.2  2d9c338bde64  3 days ago     238 MB
docker.io/grafana/grafana   7.4.0   80f8781ad990  9 days ago     168 MB
k8s.gcr.io/pause            3.2     88d88dc74807  12 months ago  330 kB
root@multi-v7-ml:~# 
```

(optional) if you want to remove the images as well:
```
root@multi-v7-ml:~# podman rmi $(podman images -a -q)
```
you might see something like this:
```
Untagged: docker.io/library/influxdb:1.8.4
Untagged: docker.io/library/telegraf:1.17.2
Untagged: docker.io/grafana/grafana:7.4.0
Deleted: b981467ac4ae2e2898af181253117226acacf5235d51bbd0942434847568d9d3
Deleted: 2d9c338bde64304bb022b962cf79b96c4effb3a6d277c23b770c0f734bab3d6d
Deleted: 80f8781ad990091fdfd5db827c1d7a2a490ac1a5b63790b8c3ff22a3188ef1f3
Error: 1 error occurred:
        * could not remove image 88d88dc74807d76e819a268700651fee3a06682d9cdb88bae124c016d66574a5 as it is being used by 1 containers: image is being used
```
confirm images:
```
root@multi-v7-ml:~# podman images
```
you might see something like this:
```
REPOSITORY        TAG     IMAGE ID      CREATED        SIZE
k8s.gcr.io/pause  3.2     88d88dc74807  12 months ago  330 kB
root@multi-v7-ml:~# 
```
Let's try now to remove the last image:
```
root@multi-v7-ml:~# podman stop $(podman ps -a -q)
```
```
adfe4c55e353ed5e104d46e54303daba3698e12c868cc7d6b1a427b5044fbd71
```
```
root@multi-v7-ml:~# podman rm -f $(podman ps --no-trunc -a -q)
```
```
Error: container adfe4c55e353ed5e104d46e54303daba3698e12c868cc7d6b1a427b5044fbd71 is the infra container of pod 04dafa1c083dca7ab422732bc667c1bf66b98ed6693001ee2a01d24aaae8918d and cannot be removed without removing the pod
```
List/remove pods:
```
root@multi-v7-ml:~# podman pod list
```
```
POD ID        NAME      STATUS  CREATED       INFRA ID      # OF CONTAINERS
04dafa1c083d  0401-tig  Exited  40 hours ago  adfe4c55e353  1
```
Remove pod:
```
root@multi-v7-ml:~# podman pod rm 0401-tig
```
```
04dafa1c083dca7ab422732bc667c1bf66b98ed6693001ee2a01d24aaae8918d
```

Let's try now to remove the last image:
```
root@multi-v7-ml:~# podman rmi $(podman images -a -q)
```
```
Untagged: k8s.gcr.io/pause:3.2
Deleted: 88d88dc74807d76e819a268700651fee3a06682d9cdb88bae124c016d66574a5
```

Let's check again:
```
root@multi-v7-ml:~# podman pod list
```
```
POD ID  NAME    STATUS  CREATED  INFRA ID  # OF CONTAINERS
```
```
root@multi-v7-ml:~# podman ps -a  
```
```
CONTAINER ID  IMAGE   COMMAND  CREATED  STATUS  PORTS   NAMES
```
```
root@multi-v7-ml:~# podman images
```
```
REPOSITORY  TAG     IMAGE ID  CREATED  SIZE
root@multi-v7-ml:~# 
```

## basic tests

### `pull`

on target:
```
root@multi-v7-ml:~# podman pull busybox
```

I picked `docker.io/library/busybox:latest`

you should see something like this:
```
✔ docker.io/library/busybox:latest
Trying to pull docker.io/library/busybox:latest...
Getting image source signatures
Copying blob f91c89ecad59 done  
Copying config d3fde6046e done  
Writing manifest to image destination
Storing signatures
d3fde6046eed872276c4b8bdfedf5f8bdee3f39878eb2304949ab69beea43b74
```

### `run` interactive

on target:

```
root@multi-v7-ml:~# podman run -it --entrypoint /bin/sh busybox
```

you should see something like this:
```
[ 1604.295999] IPv6: ADDRCONF(NETDEV_CHANGE): vethfd66c012: link becomes ready
[ 1604.303349] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[ 1604.312267] cni-podman0: port 1(vethfd66c012) entered blocking state
[ 1604.318660] cni-podman0: port 1(vethfd66c012) entered disabled state
[ 1604.325542] device vethfd66c012 entered promiscuous mode
[ 1604.331278] cni-podman0: port 1(vethfd66c012) entered blocking state
[ 1604.337684] cni-podman0: port 1(vethfd66c012) entered forwarding state
[ 1606.244020] cgroup: cgroup: disabling cgroup2 socket matching due to net_prio or net_cls activation
/ # 
```

## networking

### is `host network` accessible `from container`?

on target:
```
root@multi-v7-ml:~# ip addr
```
you should see something like this:
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 50:2d:f4:16:2d:bd brd ff:ff:ff:ff:ff:ff
    inet 192.168.42.11/24 brd 192.168.42.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::522d:f4ff:fe16:2dbd/64 scope link 
       valid_lft forever preferred_lft forever
3: sit0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1000
    link/sit 0.0.0.0 brd 0.0.0.0
4: can0: <NOARP40000> mtu 16 qdisc noop qlen 10
    link/[280] 
5: cni-podman0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue qlen 1000
    link/ether 76:1f:e9:9a:79:03 brd ff:ff:ff:ff:ff:ff
    inet 10.88.0.1/16 brd 10.88.255.255 scope global cni-podman0
       valid_lft forever preferred_lft forever
    inet6 fe80::741f:e9ff:fe9a:7903/64 scope link 
       valid_lft forever preferred_lft forever
6: vethfd66c012@can0: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue master cni-podman0 
    link/ether 1e:d9:f1:31:f9:35 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::1cd9:f1ff:fe31:f935/64 scope link 
       valid_lft forever preferred_lft forever
```
on target:
```
root@multi-v7-ml:~# route -n
```
you should see something like this:
```
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.42.254  0.0.0.0         UG    0      0        0 eth0
10.88.0.0       0.0.0.0         255.255.0.0     U     0      0        0 cni-podman0
192.168.42.0    0.0.0.0         255.255.255.0   U     0      0        0 eth0
```

in container:
```
/ # ip addr
```
you should see something like this:
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: sit0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1000
    link/sit 0.0.0.0 brd 0.0.0.0
4: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue 
    link/ether 3e:8b:de:0b:61:14 brd ff:ff:ff:ff:ff:ff
    inet 10.88.0.2/16 brd 10.88.255.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::3c8b:deff:fe0b:6114/64 scope link 
       valid_lft forever preferred_lft forever
```
in container:
```
/ # route -n
```
you should see something like this:
```
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         10.88.0.1       0.0.0.0         UG    0      0        0 eth0
10.88.0.0       0.0.0.0         255.255.0.0     U     0      0        0 eth0
```
see if we can ping something on the host subnet .42 from container:

container -> outside
```
/ # ping 192.168.42.1
```
you should see something like this:
```
PING 192.168.42.1 (192.168.42.1): 56 data bytes
64 bytes from 192.168.42.1: seq=0 ttl=63 time=0.854 ms
64 bytes from 192.168.42.1: seq=1 ttl=63 time=0.428 ms
```
in container:
```
^C
```
you should see something like this:
```
--- 192.168.42.1 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 0.428/0.641/0.854 ms
```

exit the container:
```
/ # exit
```

### is `container` accessible `from the host network`?

#### we need to publish ports for this to work

on target:
```
root@multi-v7-ml:~# podman run -it --publish 8000:8000 --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 2328.011296] IPv6: ADDRCONF(NETDEV_CHANGE): vethf40fed90: link becomes ready
[ 2328.018633] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[ 2328.025934] cni-podman0: port 1(vethf40fed90) entered blocking state
[ 2328.032312] cni-podman0: port 1(vethf40fed90) entered disabled state
[ 2328.039025] device vethf40fed90 entered promiscuous mode
[ 2328.044540] cni-podman0: port 1(vethf40fed90) entered blocking state
[ 2328.050960] cni-podman0: port 1(vethf40fed90) entered forwarding state
/ # 
```

in container:

```
# create a simple `index.html` file
cd /var/www
/var/www #
/var/www # vi index.html
```

add to the `index.html`:
```
<h1> Hello Reliable Embedded Systems! </h1>
```

start up `httpd` server:
```
/bin/httpd -v -f -p 8000 -h /var/www
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-1.png "Hello Reliable Embedded Systems!")


in container you should see:
```
[::ffff:192.168.42.52]:44386: response:200
[::ffff:192.168.42.52]:44388: response:404
```
exit the container:
```
^C
/var/www # exit
[ 6695.940477] docker0: port 1(vethc1257d5) entered disabled state
[ 6695.946649] veth4b04757: renamed from eth0
[ 6696.196421] docker0: port 1(vethc1257d5) entered disabled state
[ 6696.209038] device vethc1257d5 left promiscuous mode
[ 6696.214150] docker0: port 1(vethc1257d5) entered disabled state
root@multi-v7-ml:~# 
```

## `networking` & `volumes`

### is host network accessible from container/index.html on host volume?

on target:
```
# create a simple `index.html` file
mkdir /home/root/www
vi /home/root/www/index.html
```
add to the `index.html`:
```
<h1> Hello Reliable Embedded Systems from host volume! </h1>
```
start up the container:
```
podman run -it -v /home/root/www:/var/www --publish 8000:8000 --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 2613.328337] IPv6: ADDRCONF(NETDEV_CHANGE): veth5f47e5ea: link becomes ready
[ 2613.335646] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[ 2613.344705] cni-podman0: port 1(veth5f47e5ea) entered blocking state
[ 2613.351152] cni-podman0: port 1(veth5f47e5ea) entered disabled state
[ 2613.357823] device veth5f47e5ea entered promiscuous mode
[ 2613.363435] cni-podman0: port 1(veth5f47e5ea) entered blocking state
[ 2613.369806] cni-podman0: port 1(veth5f47e5ea) entered forwarding state
/ # 
```
in container start up `httpd` server:
```
/bin/httpd -v -f -p 8000 -h /var/www
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-2.png "Hello Reliable Embedded Systems from host volume!")


in container you should see:
```
/ # /bin/httpd -v -f -p 8000 -h /var/www
[::ffff:192.168.42.52]:44452: response:200
```

exit container:
```
[::ffff:192.168.42.52]:44452: response:200
^C
/ # exit
root@multi-v7-ml:~# [ 2694.687308] cni-podman0: port 1(veth5f47e5ea) entered disabled state
[ 2694.699514] device veth5f47e5ea left promiscuous mode
[ 2694.704597] cni-podman0: port 1(veth5f47e5ea) entered disabled state
root@multi-v7-ml:~# 
```

### the same but `non-interative`

on target start non-interactive container:
```
docker run -v /home/root/www:/var/www -p 8000:8000 --entrypoint=/bin/sh busybox -c '/bin/httpd -v -f -p 8000 -h /var/www'
```
you should see something like this:
```
Emulate Docker CLI using podman. Create /etc/containers/nodocker to quiet msg.
[ 2757.802067] IPv6: ADDRCONF(NETDEV_CHANGE): veth249f819a: link becomes ready
[ 2757.809389] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[ 2757.816758] cni-podman0: port 1(veth249f819a) entered blocking state
[ 2757.823192] cni-podman0: port 1(veth249f819a) entered disabled state
[ 2757.829875] device veth249f819a entered promiscuous mode
[ 2757.835395] cni-podman0: port 1(veth249f819a) entered blocking state
[ 2757.841806] cni-podman0: port 1(veth249f819a) entered forwarding state
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-2.png "Hello Reliable Embedded Systems from host volume!")

from the container you should see:
```
[::ffff:192.168.42.52]:44500: response:200
```

Let's try to stop the container:

from another shell to the host e.g. ssh session:
```
root@multi-v7-ml:~# podman ps -a
````
you should see something like this:
```
CONTAINER ID  IMAGE                             COMMAND               CREATED       STATUS                     PORTS                   NAMES
c8877b882afb  docker.io/library/busybox:latest                        23 hours ago  Exited (0) 23 hours ago                            fervent_hermann
4c226d7d1823  docker.io/library/busybox:latest                        23 hours ago  Exited (0) 23 hours ago    0.0.0.0:8000->8000/tcp  practical_jemison
ed0dc29fd981  docker.io/library/busybox:latest                        23 hours ago  Exited (0) 23 hours ago    0.0.0.0:8000->8000/tcp  lucid_rosalind
591ccf01f53b  docker.io/library/busybox:latest                        23 hours ago  Exited (130) 23 hours ago  0.0.0.0:8000->8000/tcp  fervent_archimedes
041edd678668  docker.io/library/busybox:latest                        23 hours ago  Exited (130) 23 hours ago  0.0.0.0:8000->8000/tcp  youthful_wilbur
4a631c39b8f2  docker.io/library/busybox:latest  -c /bin/httpd -v ...  23 hours ago  Up 23 hours ago            0.0.0.0:8000->8000/tcp  gracious_gauss
root@multi-v7-ml:~# 
```

stop it:
```
root@multi-v7-ml:~# podman stop 4a631c39b8f2
4a631c39b8f2239cee51a66c85da27b5e3d287135b4f0a11de785549ba4cff94
```
confirm that it exited:
```
root@multi-v7-ml:~# podman ps -a | grep 4a631c39b8f2
4a631c39b8f2  docker.io/library/busybox:latest  -c /bin/httpd -v ...  23 hours ago  Exited (137) About a minute ago  0.0.0.0:8000->8000/tcp  gracious_gauss
root@multi-v7-ml:~# 
```

## clean up

```
root@multi-v7-ml:~# podman stop $(podman ps -a -q)
```
```
c8877b882afb14b7e0fa38be3d1bf4e6f339f64ee4ad4d229aecf78b27897cb7
4c226d7d18233176a8ba327ab590cd88f220116a8d800d8d3e4ab1daac9943b9
ed0dc29fd9811ad7f7ecc928401f6297b5ecc079d11bbb41d0235228ae4e3d30
591ccf01f53bb9b3aee935c57eabe199ba9663dc51c1094808c67062044248f2
041edd678668074488bb7993078c81278ef5df001de210f49343c6daea7bdc5f
4a631c39b8f2239cee51a66c85da27b5e3d287135b4f0a11de785549ba4cff94
```
```
root@multi-v7-ml:~# podman rm -f $(podman ps --no-trunc -a -q)
```
```
ed0dc29fd9811ad7f7ecc928401f6297b5ecc079d11bbb41d0235228ae4e3d30
591ccf01f53bb9b3aee935c57eabe199ba9663dc51c1094808c67062044248f2
041edd678668074488bb7993078c81278ef5df001de210f49343c6daea7bdc5f
4a631c39b8f2239cee51a66c85da27b5e3d287135b4f0a11de785549ba4cff94
c8877b882afb14b7e0fa38be3d1bf4e6f339f64ee4ad4d229aecf78b27897cb7
4c226d7d18233176a8ba327ab590cd88f220116a8d800d8d3e4ab1daac9943b9
```
```
root@multi-v7-ml:~# podman ps -a
```
```
CONTAINER ID  IMAGE   COMMAND  CREATED  STATUS  PORTS   NAMES
root@multi-v7-ml:~# 
```
```
root@multi-v7-ml:~# podman pod list
```
```
POD ID  NAME    STATUS  CREATED  INFRA ID  # OF CONTAINERS
```
(optional) if you want to remove the images as well:
```
root@multi-v7-ml:~# podman images
```
```
REPOSITORY                 TAG     IMAGE ID      CREATED      SIZE
docker.io/library/busybox  latest  d3fde6046eed  12 days ago  1.18 MB
```
```
root@multi-v7-ml:~# podman rmi $(podman images -a -q)
```
```
Untagged: docker.io/library/busybox:latest
Deleted: d3fde6046eed872276c4b8bdfedf5f8bdee3f39878eb2304949ab69beea43b74
```
```
root@multi-v7-ml:~# podman images
```
```
REPOSITORY  TAG     IMAGE ID  CREATED  SIZE
root@multi-v7-ml:~# 
```
