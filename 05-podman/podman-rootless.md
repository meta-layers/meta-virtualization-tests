# podman related tests

I created a special non root user:

```
multi-v7-ml login: podman
Password: 
multi-v7-ml:~$ id
uid=1000(podman) gid=1000(podman) groups=1000(podman)
multi-v7-ml:~$ 
```

## clean up


## basic tests

### `pull`

on target:
```
root@multi-v7-ml:~# docker pull busybox
```

you should see something like this:
```
Using default tag: latest
latest: Pulling from library/busybox
f91c89ecad59: Pull complete 
Digest: sha256:e1488cb900233d035575f0a7787448cb1fa93bed0ccc0d4efc1963d7d72a8f17
Status: Downloaded newer image for busybox:latest
docker.io/library/busybox:latest
```

### `run` interactive

on target:
```
root@multi-v7-ml:~# docker run -it --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 4631.376215] docker0: port 1(veth828ce38) entered blocking state
[ 4631.382210] docker0: port 1(veth828ce38) entered disabled state
[ 4631.388805] device veth828ce38 entered promiscuous mode
[ 4633.309830] eth0: renamed from vethfc02a22
[ 4633.357250] IPv6: ADDRCONF(NETDEV_CHANGE): veth828ce38: link becomes ready
[ 4633.364283] docker0: port 1(veth828ce38) entered blocking state
[ 4633.370275] docker0: port 1(veth828ce38) entered forwarding state
[ 4633.376687] IPv6: ADDRCONF(NETDEV_CHANGE): docker0: link becomes ready
/ # 
```

## networking

### is `host network` accessible `from container`?

on target:
```
root@multi-v7-ml:~# ip addr
```
you should see something like this:
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 50:2d:f4:16:2d:bd brd ff:ff:ff:ff:ff:ff
    inet 192.168.42.11/24 brd 192.168.42.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::522d:f4ff:fe16:2dbd/64 scope link 
       valid_lft forever preferred_lft forever
3: sit0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1000
    link/sit 0.0.0.0 brd 0.0.0.0
4: can0: <NOARP40000> mtu 16 qdisc noop qlen 10
    link/[280] 
5: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue 
    link/ether 02:42:f9:66:a7:92 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:f9ff:fe66:a792/64 scope link 
       valid_lft forever preferred_lft forever
6: br-df16a15eb838: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue 
    link/ether 02:42:04:14:21:8d brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.1/16 brd 172.18.255.255 scope global br-df16a15eb838
       valid_lft forever preferred_lft forever
    inet6 fe80::42:4ff:fe14:218d/64 scope link 
       valid_lft forever preferred_lft forever
10: veth2f48c50@if9: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue master br-df16a15eb838 
    link/ether e2:df:23:9b:1e:78 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::e0df:23ff:fe9b:1e78/64 scope link 
       valid_lft forever preferred_lft forever
12: veth828ce38@if11: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue master docker0 
    link/ether 9e:e8:50:74:47:39 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::9ce8:50ff:fe74:4739/64 scope link 
       valid_lft forever preferred_lft forever
```
on target:
```
root@multi-v7-ml:~# route -n
```
you should see something like this:
```
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.42.254  0.0.0.0         UG    0      0        0 eth0
172.17.0.0      0.0.0.0         255.255.0.0     U     0      0        0 docker0
172.18.0.0      0.0.0.0         255.255.0.0     U     0      0        0 br-df16a15eb838
192.168.42.0    0.0.0.0         255.255.255.0   U     0      0        0 eth0
```

in container:
```
/ # ip addr
```
you should see something like this:
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: sit0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1000
    link/sit 0.0.0.0 brd 0.0.0.0
11: eth0@if12: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
in container:
```
/ # route -n
```
you should see something like this:
```
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.17.0.1      0.0.0.0         UG    0      0        0 eth0
172.17.0.0      0.0.0.0         255.255.0.0     U     0      0        0 eth0
```
see if we can ping something on the host subnet .42 from container:

container -> outside
```
/ # ping 192.168.42.1
```
you should see something like this:
```
PING 192.168.42.1 (192.168.42.1): 56 data bytes
64 bytes from 192.168.42.1: seq=0 ttl=63 time=0.854 ms
64 bytes from 192.168.42.1: seq=1 ttl=63 time=0.428 ms
```
in container:
```
^C
```
you should see something like this:
```
--- 192.168.42.1 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 0.428/0.641/0.854 ms
```

### is `container` accessible `from the host network`?

#### we meed to publish ports for this to work

on target:
```
root@multi-v7-ml:~# docker run -it --publish 8000:8000 --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 5667.968952] docker0: port 1(vethc1257d5) entered blocking state
[ 5667.974989] docker0: port 1(vethc1257d5) entered disabled state
[ 5667.981235] device vethc1257d5 entered promiscuous mode
[ 5669.927441] eth0: renamed from veth4b04757
[ 5669.966288] IPv6: ADDRCONF(NETDEV_CHANGE): vethc1257d5: link becomes ready
[ 5669.973322] docker0: port 1(vethc1257d5) entered blocking state
[ 5669.979306] docker0: port 1(vethc1257d5) entered forwarding state
/ # 
```

in container:

```
# create a simple `index.html` file
/ # cd /var/www
/var/www #
/var/www # vi index.html
```

add to the `index.html`:
```
<h1> Hello Reliable Embedded Systems! </h1>
```

start up `httpd` server:
```
/var/www # /bin/httpd -v -f -p 8000 -h /var/www
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-1.png "Hello Reliable Embedded Systems!")


in container you should see:
```
[::ffff:192.168.42.52]:46862: response:200
[::ffff:192.168.42.52]:46864: response:404
```
exit the container:
```
^C
/var/www # exit
[ 6695.940477] docker0: port 1(vethc1257d5) entered disabled state
[ 6695.946649] veth4b04757: renamed from eth0
[ 6696.196421] docker0: port 1(vethc1257d5) entered disabled state
[ 6696.209038] device vethc1257d5 left promiscuous mode
[ 6696.214150] docker0: port 1(vethc1257d5) entered disabled state
root@multi-v7-ml:~# 
```

## `networking` & `volumes`

### is host network accessible from container/index.html on host volume?

on target:
```
# create a simple `index.html` file
mkdir /home/root/www
vi /home/root/www/index.html
```
add to the `index.html`:
```
<h1> Hello Reliable Embedded Systems from host volume! </h1>
```
start up the container:
```
root@multi-v7-ml:~# docker run -it -v /home/root/www:/var/www --publish 8000:8000 --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 6970.272233] docker0: port 1(vethf0b2f51) entered blocking state
[ 6970.278239] docker0: port 1(vethf0b2f51) entered disabled state
[ 6970.284503] device vethf0b2f51 entered promiscuous mode
[ 6972.369720] eth0: renamed from vethac352a3
[ 6972.428685] IPv6: ADDRCONF(NETDEV_CHANGE): vethf0b2f51: link becomes ready
[ 6972.435762] docker0: port 1(vethf0b2f51) entered blocking state
[ 6972.441741] docker0: port 1(vethf0b2f51) entered forwarding state
/ # 
```
in container start up `httpd` server:
```
/bin/httpd -v -f -p 8000 -h /var/www
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-2.png "Hello Reliable Embedded Systems from host volume!")


in container you should see:
```
/ # /bin/httpd -v -f -p 8000 -h /var/www
[::ffff:192.168.42.52]:51836: response:200
```

exit container:
```
[::ffff:192.168.42.52]:51836: response:200
^C
/ # exit
[ 7261.244584] docker0: port 1(vethf0b2f51) entered disabled state
[ 7261.251243] vethac352a3: renamed from eth0
[ 7261.513230] docker0: port 1(vethf0b2f51) entered disabled state
[ 7261.525607] device vethf0b2f51 left promiscuous mode
[ 7261.530624] docker0: port 1(vethf0b2f51) entered disabled state
root@multi-v7-ml:~# 
```


### the same but `non-interative`

on target start non-interactive container:
```
root@multi-v7-ml:~# docker run -v /home/root/www:/var/www -p 8000:8000 --entrypoint=/bin/sh busybox -c '/bin/httpd -v -f -p 8000 -h /var/www'
```
you should see something like this:
```
[ 7442.632021] docker0: port 1(veth3ffb9da) entered blocking state
[ 7442.638019] docker0: port 1(veth3ffb9da) entered disabled state
[ 7442.644503] device veth3ffb9da entered promiscuous mode
[ 7445.022116] eth0: renamed from vetha42ec21
[ 7445.069305] IPv6: ADDRCONF(NETDEV_CHANGE): veth3ffb9da: link becomes ready
[ 7445.076345] docker0: port 1(veth3ffb9da) entered blocking state
[ 7445.082336] docker0: port 1(veth3ffb9da) entered forwarding state
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-2.png "Hello Reliable Embedded Systems from host volume!")

from the container you should see:
```
[::ffff:192.168.42.52]:53314: response:200
```

Let's try to stop the container:

from another shell to the host e.g. ssh session:
```
root@multi-v7-ml:~# docker ps -a
```
you should see something like this:
```
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS                        PORTS                    NAMES
25f1bd9187e5   busybox                 "/bin/sh -c '/bin/ht…"   2 minutes ago    Up 2 minutes                  0.0.0.0:8000->8000/tcp   dreamy_lamport
```

stop it:
```
root@multi-v7-ml:~# docker stop 25f1bd9187e5
25f1bd9187e5
```
confirm that it exited:
```
root@multi-v7-ml:~# docker ps -a | grep 25f1bd9187e5
25f1bd9187e5   busybox                 "/bin/sh -c '/bin/ht…"   4 minutes ago    Exited (137) 14 seconds ago                            dreamy_lamport
root@multi-v7-ml:~# 
```