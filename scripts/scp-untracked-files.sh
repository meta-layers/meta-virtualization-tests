#!/bin/bash
set +x
#source ../yocto-multi-v7-ml/yocto-for-trainer/env.sh
set +x
rm -rf /tmp/untracked
mkdir /tmp/untracked
cd ../
git ls-files --others | xargs -I {} cp --parents {} /tmp/untracked
#echo "YOCTO_VERSION_NUMBER: ${YOCTO_VERSION_NUMBER}"
set -x
cd /tmp/untracked
# remove the toolchains from tarball
#find . -name "resy-*\-toolchain\-${YOCTO_VERSION_NUMBER}*" -delete
#find . -name "resy-*\-toolchain-ext-${YOCTO_VERSION_NUMBER}*" -delete
#find . -name "*\-nativesdk\-standalone\-${YOCTO_VERSION_NUMBER}.sh" -delete
#find . -type d -name "kprobe_test_session-*" -exec rm -rf {} +
#find . -type d -name "kernel_ust_test_session-*" -exec rm -rf {} +
#find . -type d -name "test_session-*" -exec rm -rf {} +
# tree after we removed
tree /tmp/untracked
tar czvf ../untracked.tar.gz .
scp ../untracked.tar.gz rber@192.168.42.52:/tmp
set +x
