#! /bin/bash

# ./sudo-rsync-remote-sstate-mirror-to-web.sh 192.168.42.102 /opt/yocto-autobuilder-volume/sstate/ /var/www/html/sstate_mirror_warrior

HERE=$(pwd)

#MIRROR=/work/misc/Poky/sources

USER="student"
IP=$1
SRC=$2
MIRROR=$3

echo "we'll rsync ${USER}@${IP}:/$SRC to $MIRROR" 
echo "press <ENTER> to go on"
read r

cd $SRC
#sudo rsync -avz . ${MIRROR}
sudo rsync -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress ${USER}@${IP}:/${SRC} ${MIRROR}
cd ${HERE}
