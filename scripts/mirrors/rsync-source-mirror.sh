#! /bin/bash

#MIRROR=/work/misc/Poky/sources

SRC=$1
MIRROR=$2

echo "we'll rsync $SRC to $MIRROR" 
echo "press <ENTER> to go on"
read r

cd $SRC
find . -maxdepth 1 -type f | sed -e 's;^\./;;' | grep -v '.lock$' | grep -v '.done$' >/tmp/files.$$
LEN=`ls -l /tmp/files.$$ | awk '{print $5}'`
if [ "${LEN}" == "0" ]; then
    exit 0
fi
rsync -auv --files-from=/tmp/files.$$ . ${MIRROR}
if [ $? == 0 ]; then
    read -p "Upload successful - purge files(y/n)? " PURGE
    if [ "${PURGE}" == "y" ]; then
        xargs -n1 -t rm </tmp/files.$$
        xargs -n1 -t -I\{} ln -s ${MIRROR}/\{} . </tmp/files.$$
    fi
fi
