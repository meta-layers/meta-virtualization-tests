#! /bin/bash

HERE=$(pwd)

#MIRROR=/work/misc/Poky/sources

SRC=$1
MIRROR=$2

echo "we'll rsync $SRC to $MIRROR" 
echo "press <ENTER> to go on"
read r

cd $SRC
sudo rsync -avz . ${MIRROR}
cd ${HERE}
