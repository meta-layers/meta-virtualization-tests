DL="www.yoctoproject.org/docs/latest/ref-manual/ref-manual.html"

rm -rf test
mkdir test
cd test
wget -E -H -k -K -p http://${DL}
ls ${DL}
pandoc -f html -t latex ${DL} -o ref-manual.tex
