DL="www.yoctoproject.org/docs/latest/bitbake-user-manual/bitbake-user-manual.html"
#DL="www.yoctoproject.org/docs/2.0/bitbake-user-manual/bitbake-user-manual.html"

rm -rf test
mkdir test
cd test
wget -E -H -k -K -p http://${DL}
ls ${DL}
pandoc -f html -t latex ${DL} -o bitbake-manual.tex
