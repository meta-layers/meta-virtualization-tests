# docker related tests

## very basic tests

on target:
```
root@raspberrypi4-64:~# docker run hello-world
```

you should see something like this:
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
109db8fad215: Pull complete
Digest: sha256:df5f5184104426b65967e016ff2ac0bfcd44ad7899ca3bbcf8e44e4461491a9e
Status: Downloaded newer image for hello-world:latest
[  944.650014] docker0: port 1(vethb41ae12) entered blocking state
[  944.656098] docker0: port 1(vethb41ae12) entered disabled state
[  944.662797] device vethb41ae12 entered promiscuous mode
[  944.668339] audit: type=1700 audit(1627720132.020:49): dev=vethb41ae12 prom=256 old_prom=0 auid=4294967295 uid=0 gid=0 ses=4294967295
[  945.490675] eth0: renamed from veth1ccb9c2
[  945.515101] IPv6: ADDRCONF(NETDEV_CHANGE): vethb41ae12: link becomes ready
[  945.522370] docker0: port 1(vethb41ae12) entered blocking state
[  945.528430] docker0: port 1(vethb41ae12) entered forwarding state

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (arm64v8)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/


[  945.788668] docker0: port 1(vethb41ae12) entered disabled state
[  945.795030] veth1ccb9c2: renamed from eth0
[  945.876497] docker0: port 1(vethb41ae12) entered disabled state
[  946.020649] device vethb41ae12 left promiscuous mode
[  946.025854] audit: type=1700 audit(1627720133.236:50): dev=vethb41ae12 prom=0 old_prom=256 auid=4294967295 uid=0 gid=0 ses=4294967295
[  946.038246] docker0: port 1(vethb41ae12) entered disabled state
```

on target:
```
root@raspberrypi4-64:~# docker run -it ubuntu bash
```

you should see something like this:
```
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
be0de17fe24f: Pull complete
Digest: sha256:82becede498899ec668628e7cb0ad87b6e1c371cb8a1e597d83a47fac21d6af3
Status: Downloaded newer image for ubuntu:latest
[ 1014.950017] docker0: port 1(veth01f37ca) entered blocking state
[ 1014.956152] docker0: port 1(veth01f37ca) entered disabled state
[ 1014.962605] device veth01f37ca entered promiscuous mode
[ 1014.968975] audit: type=1700 audit(1627720202.320:51): dev=veth01f37ca prom=256 old_prom=0 auid=4294967295 uid=0 gid=0 ses=4294967295
[ 1015.856007] eth0: renamed from veth96dc39b
[ 1015.874522] IPv6: ADDRCONF(NETDEV_CHANGE): veth01f37ca: link becomes ready
[ 1015.881800] docker0: port 1(veth01f37ca) entered blocking state
[ 1015.887874] docker0: port 1(veth01f37ca) entered forwarding state
root@f019f5eaa3c9:/# ls
bin  boot  dev  etc  home  lib  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@f019f5eaa3c9:/# exit
exit
[ 1046.843028] docker0: port 1(veth01f37ca) entered disabled state
[ 1046.850086] veth96dc39b: renamed from eth0
[ 1046.934148] docker0: port 1(veth01f37ca) entered disabled state
[ 1047.072746] device veth01f37ca left promiscuous mode
[ 1047.077909] audit: type=1700 audit(1627720234.299:52): dev=veth01f37ca prom=0 old_prom=256 auid=4294967295 uid=0 gid=0 ses=4294967295
[ 1047.081687] docker0: port 1(veth01f37ca) entered disabled state
```

## basic tests

### `pull`


on target:
```
root@multi-v7-ml:~# docker pull busybox
```

you should see something like this:
```
Using default tag: latest
latest: Pulling from library/busybox
f91c89ecad59: Pull complete 
Digest: sha256:e1488cb900233d035575f0a7787448cb1fa93bed0ccc0d4efc1963d7d72a8f17
Status: Downloaded newer image for busybox:latest
docker.io/library/busybox:latest
```

### `run` interactive

on target:
```
root@multi-v7-ml:~# docker run -it --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 4631.376215] docker0: port 1(veth828ce38) entered blocking state
[ 4631.382210] docker0: port 1(veth828ce38) entered disabled state
[ 4631.388805] device veth828ce38 entered promiscuous mode
[ 4633.309830] eth0: renamed from vethfc02a22
[ 4633.357250] IPv6: ADDRCONF(NETDEV_CHANGE): veth828ce38: link becomes ready
[ 4633.364283] docker0: port 1(veth828ce38) entered blocking state
[ 4633.370275] docker0: port 1(veth828ce38) entered forwarding state
[ 4633.376687] IPv6: ADDRCONF(NETDEV_CHANGE): docker0: link becomes ready
/ # 
```

--> raspi4-64 workaround #############################################################################

Please note, that at the moment an upstream `meta-raspberrypi/raspberrypi4-64` with `busybox` crashes:

```
[ 4037.908447] audit: type=1701 audit(1627723225.241:63): auid=4294967295 uid=0 gid=0 ses=4294967295 pid=1860 comm="sh" exe="/bin/sh" sig=11 res=1
/ # [ 4038.117271] docker0: port 1(veth4a4a213) entered disabled state
```

a possible workaround is:

```
docker pull busybox:glibc
docker run -it --entrypoint /bin/sh busybox:glibc
```

and you should see:

```
[ 4616.081984] docker0: port 1(vethdb5bd0d) entered blocking state
[ 4616.088099] docker0: port 1(vethdb5bd0d) entered disabled state
[ 4616.096725] device vethdb5bd0d entered promiscuous mode
[ 4616.102269] audit: type=1700 audit(1627723803.404:65): dev=vethdb5bd0d prom=256 old_prom=0 auid=4294967295 uid=0 gid=0 ses=4294967295
[ 4616.999016] eth0: renamed from vethc4655f3
[ 4617.027950] IPv6: ADDRCONF(NETDEV_CHANGE): vethdb5bd0d: link becomes ready
[ 4617.035207] docker0: port 1(vethdb5bd0d) entered blocking state
[ 4617.041263] docker0: port 1(vethdb5bd0d) entered forwarding state
/ # ls
bin    dev    etc    home   lib    lib64  proc   root   sys    tmp    usr    var
/ #
```

<-- raspi4-64 workaround #############################################################################

## networking

### is `host network` accessible `from container`?

on target:
```
root@multi-v7-ml:~# ip addr
```
you should see something like this:
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 50:2d:f4:16:2d:bd brd ff:ff:ff:ff:ff:ff
    inet 192.168.42.11/24 brd 192.168.42.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::522d:f4ff:fe16:2dbd/64 scope link 
       valid_lft forever preferred_lft forever
3: sit0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1000
    link/sit 0.0.0.0 brd 0.0.0.0
4: can0: <NOARP40000> mtu 16 qdisc noop qlen 10
    link/[280] 
5: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue 
    link/ether 02:42:f9:66:a7:92 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:f9ff:fe66:a792/64 scope link 
       valid_lft forever preferred_lft forever
6: br-df16a15eb838: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue 
    link/ether 02:42:04:14:21:8d brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.1/16 brd 172.18.255.255 scope global br-df16a15eb838
       valid_lft forever preferred_lft forever
    inet6 fe80::42:4ff:fe14:218d/64 scope link 
       valid_lft forever preferred_lft forever
10: veth2f48c50@if9: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue master br-df16a15eb838 
    link/ether e2:df:23:9b:1e:78 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::e0df:23ff:fe9b:1e78/64 scope link 
       valid_lft forever preferred_lft forever
12: veth828ce38@if11: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue master docker0 
    link/ether 9e:e8:50:74:47:39 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::9ce8:50ff:fe74:4739/64 scope link 
       valid_lft forever preferred_lft forever
```
on target:
```
root@multi-v7-ml:~# route -n
```
you should see something like this:
```
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.42.254  0.0.0.0         UG    0      0        0 eth0
172.17.0.0      0.0.0.0         255.255.0.0     U     0      0        0 docker0
172.18.0.0      0.0.0.0         255.255.0.0     U     0      0        0 br-df16a15eb838
192.168.42.0    0.0.0.0         255.255.255.0   U     0      0        0 eth0
```

in container:
```
/ # ip addr
```
you should see something like this:
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: sit0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1000
    link/sit 0.0.0.0 brd 0.0.0.0
11: eth0@if12: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
in container:
```
/ # route -n
```
you should see something like this:
```
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.17.0.1      0.0.0.0         UG    0      0        0 eth0
172.17.0.0      0.0.0.0         255.255.0.0     U     0      0        0 eth0
```
see if we can ping something on the host subnet .42 from container:

container -> outside
```
/ # ping 192.168.42.1
```
you should see something like this:
```
PING 192.168.42.1 (192.168.42.1): 56 data bytes
64 bytes from 192.168.42.1: seq=0 ttl=63 time=0.854 ms
64 bytes from 192.168.42.1: seq=1 ttl=63 time=0.428 ms
```
in container:
```
^C
```
you should see something like this:
```
--- 192.168.42.1 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 0.428/0.641/0.854 ms
```

exit the container:
```
/ # exit
```

### is `host network` accessible `by name` ?

on target: 
```
root@multi-v7-ml:~# nslookup pfsense.res.training
```
you should see something like this:
```
Server:    192.168.42.254
Address 1: 192.168.42.254 _gateway

Name:      pfsense.res.training
Address 1: 192.168.42.254 _gateway
```

### is `host network` accessible `from container` `by name` ?

in container: 
```
/ # nslookup pfsense.res.training
```
you should see something like this:
```
Server:    192.168.42.254
Address 1: 192.168.42.254 pfsense.res.training

Name:      pfsense.res.training
Address 1: 192.168.42.254 pfsense.res.training
```
### is `outside network` accessible `by name` ?

on target: 
```
root@multi-v7-ml:~# nslookup www.google.com
```
you should see something like this:
```
Server:    192.168.42.254
Address 1: 192.168.42.254 _gateway

Name:      www.google.com
Address 1: 142.250.186.68 fra24s05-in-f4.1e100.net
Address 2: 2a00:1450:4001:828::2004 fra24s05-in-x04.1e100.net
```

### is `outside network` accessible `from container` `by name` ?

in container: 
```
/ # nslookup www.google.com
```
you should see something like this:
```
Server:    192.168.42.254
Address 1: 192.168.42.254 pfsense.res.training

Name:      www.google.com
Address 1: 142.250.186.68 fra24s05-in-f4.1e100.net
Address 2: 2a00:1450:4001:828::2004 fra24s05-in-x04.1e100.net
```

### is `container` accessible `from the host network`?

#### we need to publish ports for this to work

on target:
```
root@multi-v7-ml:~# docker run -it --publish 8000:8000 --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 5667.968952] docker0: port 1(vethc1257d5) entered blocking state
[ 5667.974989] docker0: port 1(vethc1257d5) entered disabled state
[ 5667.981235] device vethc1257d5 entered promiscuous mode
[ 5669.927441] eth0: renamed from veth4b04757
[ 5669.966288] IPv6: ADDRCONF(NETDEV_CHANGE): vethc1257d5: link becomes ready
[ 5669.973322] docker0: port 1(vethc1257d5) entered blocking state
[ 5669.979306] docker0: port 1(vethc1257d5) entered forwarding state
/ # 
```

in container:

```
# create a simple `index.html` file
/ # cd /var/www
/var/www #
/var/www # vi index.html
```

add to the `index.html`:
```
<h1> Hello Reliable Embedded Systems! </h1>
```

start up `httpd` server:
```
/var/www # /bin/httpd -v -f -p 8000 -h /var/www
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-1.png "Hello Reliable Embedded Systems!")


in container you should see:
```
[::ffff:192.168.42.52]:46862: response:200
[::ffff:192.168.42.52]:46864: response:404
```
exit the container:
```
^C
/var/www # exit
[ 6695.940477] docker0: port 1(vethc1257d5) entered disabled state
[ 6695.946649] veth4b04757: renamed from eth0
[ 6696.196421] docker0: port 1(vethc1257d5) entered disabled state
[ 6696.209038] device vethc1257d5 left promiscuous mode
[ 6696.214150] docker0: port 1(vethc1257d5) entered disabled state
root@multi-v7-ml:~# 
```

## `networking` & `volumes`

### is host network accessible from container/index.html on host volume?

on target:
```
# create a simple `index.html` file
mkdir /home/root/www
vi /home/root/www/index.html
```
add to the `index.html`:
```
<h1> Hello Reliable Embedded Systems from host volume! </h1>
```
start up the container:
```
root@multi-v7-ml:~# docker run -it -v /home/root/www:/var/www --publish 8000:8000 --entrypoint /bin/sh busybox
```
you should see something like this:
```
[ 6970.272233] docker0: port 1(vethf0b2f51) entered blocking state
[ 6970.278239] docker0: port 1(vethf0b2f51) entered disabled state
[ 6970.284503] device vethf0b2f51 entered promiscuous mode
[ 6972.369720] eth0: renamed from vethac352a3
[ 6972.428685] IPv6: ADDRCONF(NETDEV_CHANGE): vethf0b2f51: link becomes ready
[ 6972.435762] docker0: port 1(vethf0b2f51) entered blocking state
[ 6972.441741] docker0: port 1(vethf0b2f51) entered forwarding state
/ # 
```
in container start up `httpd` server:
```
/bin/httpd -v -f -p 8000 -h /var/www
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-2.png "Hello Reliable Embedded Systems from host volume!")


in container you should see:
```
/ # /bin/httpd -v -f -p 8000 -h /var/www
[::ffff:192.168.42.52]:51836: response:200
```

exit container:
```
[::ffff:192.168.42.52]:51836: response:200
^C
/ # exit
[ 7261.244584] docker0: port 1(vethf0b2f51) entered disabled state
[ 7261.251243] vethac352a3: renamed from eth0
[ 7261.513230] docker0: port 1(vethf0b2f51) entered disabled state
[ 7261.525607] device vethf0b2f51 left promiscuous mode
[ 7261.530624] docker0: port 1(vethf0b2f51) entered disabled state
root@multi-v7-ml:~# 
```


### the same but `non-interative`

on target start non-interactive container:
```
root@multi-v7-ml:~# docker run -v /home/root/www:/var/www -p 8000:8000 --entrypoint=/bin/sh busybox -c '/bin/httpd -v -f -p 8000 -h /var/www'
```
you should see something like this:
```
[ 7442.632021] docker0: port 1(veth3ffb9da) entered blocking state
[ 7442.638019] docker0: port 1(veth3ffb9da) entered disabled state
[ 7442.644503] device veth3ffb9da entered promiscuous mode
[ 7445.022116] eth0: renamed from vetha42ec21
[ 7445.069305] IPv6: ADDRCONF(NETDEV_CHANGE): veth3ffb9da: link becomes ready
[ 7445.076345] docker0: port 1(veth3ffb9da) entered blocking state
[ 7445.082336] docker0: port 1(veth3ffb9da) entered forwarding state
```

From some machine on your network (e.g. host) access with a web browser:
```
http://<ip-address-of-your-target>:8000
```

On the browser you should see:

![alt text](img/hello-res-2.png "Hello Reliable Embedded Systems from host volume!")

from the container you should see:
```
[::ffff:192.168.42.52]:53314: response:200
```

Let's try to stop the container:

from another shell to the host e.g. ssh session:
```
root@multi-v7-ml:~# docker ps -a
```
you should see something like this:
```
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS                        PORTS                    NAMES
25f1bd9187e5   busybox                 "/bin/sh -c '/bin/ht…"   2 minutes ago    Up 2 minutes                  0.0.0.0:8000->8000/tcp   dreamy_lamport
```

stop it:
```
root@multi-v7-ml:~# docker stop 25f1bd9187e5
25f1bd9187e5
```
confirm that it exited:
```
root@multi-v7-ml:~# docker ps -a | grep 25f1bd9187e5
25f1bd9187e5   busybox                 "/bin/sh -c '/bin/ht…"   4 minutes ago    Exited (137) 14 seconds ago                            dreamy_lamport
root@multi-v7-ml:~# 
```
