# Kubernetes - Web UI (Dashboard) 

## Install the Web UI (Dashboard)

on target/host-1:
```
root@k3s-host-1:~# kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml
```

you should see something like this:
```
namespace/kubernetes-dashboard created
serviceaccount/kubernetes-dashboard created
service/kubernetes-dashboard created
secret/kubernetes-dashboard-certs created
secret/kubernetes-dashboard-csrf created
secret/kubernetes-dashboard-key-holder created
configmap/kubernetes-dashboard-settings created
role.rbac.authorization.k8s.io/kubernetes-dashboard created
clusterrole.rbac.authorization.k8s.io/kubernetes-dashboard created
rolebinding.rbac.authorization.k8s.io/kubernetes-dashboard created
clusterrolebinding.rbac.authorization.k8s.io/kubernetes-dashboard created
deployment.apps/kubernetes-dashboard created
service/dashboard-metrics-scraper created
deployment.apps/dashboard-metrics-scraper created
```

on target/host-1:
```
root@k3s-host-1:~# kubectl describe nodes
```

you should see something like this:
```
Name:               k3s-host-1                                                                                                                                                                
Roles:              control-plane,master                                                                                                                                                      
Labels:             beta.kubernetes.io/arch=arm64                                                                                                                                             
                    beta.kubernetes.io/instance-type=k3s                                                                                                                                      
                    beta.kubernetes.io/os=linux                                                                                                                                               
                    kubernetes.io/arch=arm64                                                                                                                                                  
                    kubernetes.io/hostname=k3s-host-1                                                                                                                                         
                    kubernetes.io/os=linux                                                                                                                                                    
                    node-role.kubernetes.io/control-plane=true                                                                                                                                
                    node-role.kubernetes.io/master=true                                                                                                                                       
                    node.kubernetes.io/instance-type=k3s                                                                                                                                      
Annotations:        flannel.alpha.coreos.com/backend-data: {"VNI":1,"VtepMAC":"42:c5:ff:aa:7f:40"}                                                                                            
                    flannel.alpha.coreos.com/backend-type: vxlan                                                                                                                              
                    flannel.alpha.coreos.com/kube-subnet-manager: true                                                                                                                        
                    flannel.alpha.coreos.com/public-ip: 192.168.42.248                                                                                                                        
                    k3s.io/hostname: k3s-host-1                                                                                                                                               
                    k3s.io/internal-ip: 192.168.42.248                                                                                                                                        
                    k3s.io/node-args: ["server"]                                                                                                                                              
                    k3s.io/node-config-hash: MLFMUCBMRVINLJJKSG32TOUFWB4CN55GMSNY25AZPESQXZCYRN2A====                                                                                         
                    k3s.io/node-env: {}                                                                                                                                                       
                    node.alpha.kubernetes.io/ttl: 0                                                                                                                                           
                    volumes.kubernetes.io/controller-managed-attach-detach: true                                                                                                              
CreationTimestamp:  Sun, 01 Aug 2021 17:34:17 +0000                                                                                                                                           
Taints:             <none>                                                                                                                                                                    
Unschedulable:      false                                                                                                                                                                     
Lease:                                                                                                                                                                                        
  HolderIdentity:  k3s-host-1                                                                                                                                                                 
  AcquireTime:     <unset>                                                                                                                                                                    
  RenewTime:       Sun, 01 Aug 2021 19:22:15 +0000                                                                                                                                            
Conditions:                                                                                                                                                                                   
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message                                                       
  ----                 ------  -----------------                 ------------------                ------                       -------                                                       
  NetworkUnavailable   False   Sun, 01 Aug 2021 17:34:41 +0000   Sun, 01 Aug 2021 17:34:41 +0000   FlannelIsUp                  Flannel is running on this node                               
  MemoryPressure       False   Sun, 01 Aug 2021 19:21:01 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available                       
  DiskPressure         False   Sun, 01 Aug 2021 19:21:01 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure                                  
  PIDPressure          False   Sun, 01 Aug 2021 19:21:01 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available                          
  Ready                True    Sun, 01 Aug 2021 19:21:01 +0000   Sun, 01 Aug 2021 17:34:39 +0000   KubeletReady                 kubelet is posting ready status                               
Addresses:                                                                                                                                                                                    
  InternalIP:  192.168.42.248                                                                                                                                                                 
  Hostname:    k3s-host-1                                                                                                                                                                     
Capacity:                                                                                                                                                                                     
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  976568Ki                                                                                                                                                                
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
Allocatable:                                             
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  950005350                                                                                                                                                               
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
System Info:                                                                                                                                                                                  
  Machine ID:                 15484e89530c4203baee0d4d3006da31                                                                                                                                
  System UUID:                15484e89530c4203baee0d4d3006da31                                                                                                                                
  Boot ID:                    4e360e40-2e60-433e-a922-a3d2a87b0312                                                                                                                            
  Kernel Version:             5.10.31-v8                                                                                                                                                      
  OS Image:                   Resy systemd (Reliable Embedded Systems Reference Distro) 3.3+snapshot-f735627e7c5aeb421338db55f3905d74751d4b71 (olivegold)                                     
  Operating System:           linux                                                                                                                                                           
  Architecture:               arm64                                                                                                                                                           
  Container Runtime Version:  containerd://1.5.4-12-g1c13c54ca.m                                                                                                                              
  Kubelet Version:            v1.21.3-k3s1                                                                                                                                                    
  Kube-Proxy Version:         v1.21.3-k3s1                                                                                                                                                    
PodCIDR:                      10.42.0.0/24                                                                                                                                                    
PodCIDRs:                     10.42.0.0/24                                                                                                                                                    
ProviderID:                   k3s://k3s-host-1                                                                                                                                                
Non-terminated Pods:          (5 in total)                                                                                                                                                    
  Namespace                   Name                                       CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age                                                        
  ---------                   ----                                       ------------  ----------  ---------------  -------------  ---                                                        
  kube-system                 coredns-7448499f4d-6j5mw                   100m (2%)     0 (0%)      70Mi (0%)        170Mi (2%)     107m                                                       
  kube-system                 metrics-server-86cbb8457f-hcv8l            0 (0%)        0 (0%)      0 (0%)           0 (0%)         107m                                                       
  kube-system                 local-path-provisioner-5ff76fc89d-l5m7z    0 (0%)        0 (0%)      0 (0%)           0 (0%)         107m                                                       
  kube-system                 svclb-traefik-ws4ps                        0 (0%)        0 (0%)      0 (0%)           0 (0%)         105m                                                       
  kube-system                 traefik-97b44b794-rslsr                    0 (0%)        0 (0%)      0 (0%)           0 (0%)         105m                                                       
Allocated resources:                                                                                                                                                                          
  (Total limits may be over 100 percent, i.e., overcommitted.)                                                                                                                                
  Resource           Requests   Limits                                                                                                                                                        
  --------           --------   ------                                                                                                                                                        
  cpu                100m (2%)  0 (0%)                                                                                                                                                        
  memory             70Mi (0%)  170Mi (2%)                                                                                                                                                    
  ephemeral-storage  0 (0%)     0 (0%)                                                                                                                                                        
Events:              <none>                                                                                                                                                                   
                                                                                                                                                                                              
                                                                                                                                                                                              
Name:               k3s-node-1                                                                                                                                                                
Roles:              <none>                                                                                                                                                                    
Labels:             beta.kubernetes.io/arch=arm64                                                                                                                                             
                    beta.kubernetes.io/instance-type=k3s                                                                                                                                      
                    beta.kubernetes.io/os=linux                                                                                                                                               
                    kubernetes.io/arch=arm64                                                                                                                                                  
                    kubernetes.io/hostname=k3s-node-1                                                                                                                                         
                    kubernetes.io/os=linux                                                                                                                                                    
                    node.kubernetes.io/instance-type=k3s                                                                                                                                      
Annotations:        flannel.alpha.coreos.com/backend-data: {"VNI":1,"VtepMAC":"fe:82:db:3a:ab:9b"}  
                    flannel.alpha.coreos.com/backend-type: vxlan                                                                                                                              
                    flannel.alpha.coreos.com/kube-subnet-manager: true                                                                                                                        
                    flannel.alpha.coreos.com/public-ip: 192.168.42.251                                                                                                                        
                    k3s.io/hostname: k3s-node-1                                                                                                                                               
                    k3s.io/internal-ip: 192.168.42.251                                                                                                                                        
                    k3s.io/node-args: ["agent"]                                                                                                                                               
                    k3s.io/node-config-hash: 62MR7AJYILHGDTVOIFLBUA7LBNZRECSDNLCIGXK54OGTOEAS76LA====                                                                                         
                    k3s.io/node-env: {"K3S_TOKEN":"********","K3S_URL":"https://k3s-host-1.res.training:6443"}                                                                                
                    node.alpha.kubernetes.io/ttl: 0                                                                                                                                           
                    volumes.kubernetes.io/controller-managed-attach-detach: true                                                                                                              
CreationTimestamp:  Sun, 01 Aug 2021 19:06:32 +0000                                                                                                                                           
Taints:             <none>                                                                                                                                                                    
Unschedulable:      false                                                                                                                                                                     
Lease:                                                                                                                                                                                        
  HolderIdentity:  k3s-node-1                                                                                                                                                                 
  AcquireTime:     <unset>                                                                                                                                                                    
  RenewTime:       Sun, 01 Aug 2021 19:22:15 +0000                                                                                                                                            
Conditions:                                                                                                                                                                                   
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message                                                       
  ----                 ------  -----------------                 ------------------                ------                       -------                                                       
  NetworkUnavailable   False   Sun, 01 Aug 2021 19:06:34 +0000   Sun, 01 Aug 2021 19:06:34 +0000   FlannelIsUp                  Flannel is running on this node                               
  MemoryPressure       False   Sun, 01 Aug 2021 19:21:33 +0000   Sun, 01 Aug 2021 19:06:32 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available                       
  DiskPressure         False   Sun, 01 Aug 2021 19:21:33 +0000   Sun, 01 Aug 2021 19:06:32 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure                                  
  PIDPressure          False   Sun, 01 Aug 2021 19:21:33 +0000   Sun, 01 Aug 2021 19:06:32 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available                          
  Ready                True    Sun, 01 Aug 2021 19:21:33 +0000   Sun, 01 Aug 2021 19:06:42 +0000   KubeletReady                 kubelet is posting ready status                               
Addresses:                                                                                                                                                                                    
  InternalIP:  192.168.42.251                                                                                                                                                                 
  Hostname:    k3s-node-1                                                                                                                                                                     
Capacity:                                                                                                                                                                                     
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  976568Ki                                                                                                                                                                
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
Allocatable:                                                                                                                                                                                  
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  950005350                                                                                                                                                               
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
System Info:                                                                                                                                                                                  
  Machine ID:                 205cef95869541298fe5a8f6a64a872e                                                                                                                                
  System UUID:                205cef95869541298fe5a8f6a64a872e                                                                                                                                
  Boot ID:                    0751c1d6-8ee5-483c-af96-20454f863446                                                                                                                            
  Kernel Version:             5.10.31-v8                                                                                                                                                      
  OS Image:                   Resy systemd (Reliable Embedded Systems Reference Distro) 3.3+snapshot-f735627e7c5aeb421338db55f3905d74751d4b71 (olivegold)                                     
  Operating System:           linux                                                                                                                                                           
  Architecture:               arm64                                                                                                                                                           
  Container Runtime Version:  containerd://1.5.4-12-g1c13c54ca.m                                  
    Kubelet Version:            v1.21.3-k3s1                                                                                                                                                    
  Kube-Proxy Version:         v1.21.3-k3s1                                                                                                                                                    
PodCIDR:                      10.42.1.0/24                                                                                                                                                    
PodCIDRs:                     10.42.1.0/24                                                                                                                                                    
ProviderID:                   k3s://k3s-node-1                                                                                                                                                
Non-terminated Pods:          (3 in total)                                                                                                                                                    
  Namespace                   Name                                          CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age                                                     
  ---------                   ----                                          ------------  ----------  ---------------  -------------  ---                                                     
  kube-system                 svclb-traefik-stl7h                           0 (0%)        0 (0%)      0 (0%)           0 (0%)         15m                                                     
  kubernetes-dashboard        kubernetes-dashboard-67484c44f6-2ztn2         0 (0%)        0 (0%)      0 (0%)           0 (0%)         81s                                                     
  kubernetes-dashboard        dashboard-metrics-scraper-856586f554-5gbzn    0 (0%)        0 (0%)      0 (0%)           0 (0%)         81s                                                     
Allocated resources:                                                                                                                                                                          
  (Total limits may be over 100 percent, i.e., overcommitted.)                                                                                                                                
  Resource           Requests  Limits                                                                                                                                                         
  --------           --------  ------                                                                                                                                                         
  cpu                0 (0%)    0 (0%)                                                                                                                                                         
  memory             0 (0%)    0 (0%)                                                                                                                                                         
  ephemeral-storage  0 (0%)    0 (0%)                                                                                                                                                         
Events:                                                                                                                                                                                       
  Type     Reason                   Age   From        Message                                                                                                                                 
  ----     ------                   ----  ----        -------                                                                                                                                 
  Normal   Starting                 15m   kubelet     Starting kubelet.                                                                                                                       
  Warning  InvalidDiskCapacity      15m   kubelet     invalid capacity 0 on image filesystem                                                                                                  
  Normal   NodeHasSufficientMemory  15m   kubelet     Node k3s-node-1 status is now: NodeHasSufficientMemory                                                                                  
  Normal   NodeHasNoDiskPressure    15m   kubelet     Node k3s-node-1 status is now: NodeHasNoDiskPressure                                                                                    
  Normal   NodeHasSufficientPID     15m   kubelet     Node k3s-node-1 status is now: NodeHasSufficientPID                                                                                     
  Normal   NodeAllocatableEnforced  15m   kubelet     Updated Node Allocatable limit across pods                                                                                              
  Normal   Starting                 15m   kube-proxy  Starting kube-proxy.                                                                                                                    
  Normal   NodeReady                15m   kubelet     Node k3s-node-1 status is now: NodeReady                     

```

on target/host-1:
```
root@k3s-host-1:~# kubectl get pods --all-namespaces
```

you should see something like this:
```
NAMESPACE              NAME                                         READY   STATUS      RESTARTS   AGE
kube-system            coredns-7448499f4d-6j5mw                     1/1     Running     0          111m
kube-system            metrics-server-86cbb8457f-hcv8l              1/1     Running     0          111m
kube-system            local-path-provisioner-5ff76fc89d-l5m7z      1/1     Running     0          111m
kube-system            helm-install-traefik-crd-f4nlx               0/1     Completed   0          111m
kube-system            helm-install-traefik-fchrr                   0/1     Completed   1          111m
kube-system            svclb-traefik-ws4ps                          2/2     Running     0          108m
kube-system            traefik-97b44b794-rslsr                      1/1     Running     0          108m
kube-system            svclb-traefik-stl7h                          2/2     Running     0          19m
kubernetes-dashboard   dashboard-metrics-scraper-856586f554-5gbzn   1/1     Running     0          4m56s
kubernetes-dashboard   kubernetes-dashboard-67484c44f6-2ztn2        1/1     Running     0          4m56s
```

on target/host-1:
```
root@k3s-host-1:~# kubectl get services --all-namespaces
```
you should see something like this:
```
NAMESPACE              NAME                        TYPE           CLUSTER-IP      EXTERNAL-IP                     PORT(S)                      AGE
default                kubernetes                  ClusterIP      10.43.0.1       <none>                          443/TCP                      112m
kube-system            kube-dns                    ClusterIP      10.43.0.10      <none>                          53/UDP,53/TCP,9153/TCP       111m
kube-system            metrics-server              ClusterIP      10.43.195.17    <none>                          443/TCP                      111m
kube-system            traefik                     LoadBalancer   10.43.64.181    192.168.42.248,192.168.42.251   80:32266/TCP,443:30112/TCP   109m
kubernetes-dashboard   kubernetes-dashboard        ClusterIP      10.43.156.74    <none>                          443/TCP                      5m26s
kubernetes-dashboard   dashboard-metrics-scraper   ClusterIP      10.43.204.149   <none>                          8000/TCP                     5m25s
```

## More `kubernetes-dashboard` setup

### dashboard admin

on target/host-1:
```
root@k3s-host-1:~# mkdir ~/dashboard && cd ~/dashboard
root@k3s-host-1:~/dashboard# vim dashboard-admin.txt
```

add this:
```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
```

on target/host-1:
```
root@k3s-host-1:~/dashboard# mv dashboard-admin.txt dashboard-admin.yaml
root@k3s-host-1:~/dashboard# kubectl apply -f dashboard-admin.yaml
```

you should see something like this:
```
serviceaccount/admin-user created
clusterrolebinding.rbac.authorization.k8s.io/admin-user created
```

get the admin token:
```
root@k3s-host-1:~/dashboard# kubectl get secret -n kubernetes-dashboard $(kubectl get serviceaccount admin-user -n kubernetes-dashboard -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode
```
you should get something like this:
```
eyJhbGciOiJSUzI1NiIsImtpZCI6Ill4cDVraUl1eTBOSjBTZERLT3RKbHNmNVE0SUZQWGtRRHlYUnpXQ1ZXcWcifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLTZtcWtsIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI0NjEyZGVkMy0wZTY2LTQ0NWEtYmZkMC00N2MzMTEzNmUxM2EiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6YWRtaW4tdXNlciJ9.pBzxSPm33jTdsRoXCqGySBU84v9M7tV_hcW7NTdZ_Nev9MdvfXqr5s-vaXCYDWP1WbuJq4fmMD8A0J7xV-zagwlWSf4ObUg3q-5U2LFv4bYzR7cVjEQblZSn1G28Ao4n8ddUVou3GEvrzhBHXd35X0iRxAXyVGPRpnyiiAZWiM40KbYeNbdSGnmJXMZOyOrDZBtAA_eYqhkEveSwU6iKPR4YKh6V0eanR1wG1O52XWGkZzmiEe1sfbIcqt4lsP2DjGQ8ECnhDbVv8EumyaiXAM8qS-nL7_f9S527c7EcRD5quhnlRIdrhGJlED0rJYqOZVCLcZEaQcw5IeemTwfKhw
root@k3s-host-1:~/dashboard#
```


### dashboard read-only user

on target/host-1:
```
root@k3s-host-1:~/dashboard# vim dashboard-read-only.txt
```

add this:
```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: read-only-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  labels:
  name: read-only-clusterrole
  namespace: default
rules:
- apiGroups:
  - ""
  resources: ["*"]
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - extensions
  resources: ["*"]
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - apps
  resources: ["*"]
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: read-only-binding
roleRef:
  kind: ClusterRole
  name: read-only-clusterrole
  apiGroup: rbac.authorization.k8s.io
subjects:
- kind: ServiceAccount
  name: read-only-user
  namespace: kubernetes-dashboard
```

on target/host-1:
```
root@k3s-host-1:~/dashboard# mv dashboard-read-only.txt dashboard-read-only.yaml
root@k3s-host-1:~/dashboard# kubectl apply -f dashboard-read-only.yaml
```

you should see something like this:
```
serviceaccount/read-only-user created
clusterrole.rbac.authorization.k8s.io/read-only-clusterrole created
clusterrolebinding.rbac.authorization.k8s.io/read-only-binding created
```

get the read-only user token:
```
root@k3s-host-1:~/dashboard# kubectl get secret -n kubernetes-dashboard $(kubectl get serviceaccount read-only-user -n kubernetes-dashboard -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode
```
you should get something like this:
```
eyJhbGciOiJSUzI1NiIsImtpZCI6Ill4cDVraUl1eTBOSjBTZERLT3RKbHNmNVE0SUZQWGtRRHlYUnpXQ1ZXcWcifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJyZWFkLW9ubHktdXNlci10b2tlbi0ybXRybCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJyZWFkLW9ubHktdXNlciIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjM4NzQwY2RhLTY5YjEtNDJlZS1hYzg3LTQwMzRmMjM4NDFiYSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlcm5ldGVzLWRhc2hib2FyZDpyZWFkLW9ubHktdXNlciJ9.aIx9zrDAMrSJswmqLteKDvbCEmGRw6lpCbmLV7NhzEfiUcjscM5bvdMiOw75VSlDTTqzSKKQtIl_UNFrnvNNdFMtcY3zy9Qa7jKd820OdAFThmP2IGH2Az6NHwQD8bH0zPdvTARxdsYmMVAXIeq9opxBnXNigBdSfcr91MTHrOmrxQ94_l8DAA8mjwFfJpMaO1be4IzDa29ENxF0YsZHkiayji_TNqcZRebdgDVosNmVNkTFqxGbWEXBvbC8YpTqxZd1ALYbOpb_QkS50ynJHri9dGXENd26p4E95c-F1Mea7iThCjTSZeYShU02UbnzLVoqmFNyyRPN-R2iFAJD8A
root@k3s-host-1:~/dashboard#
```

As this point the `kubernetes-dashboard` is running, but it's not accessible from an external ip.

## Make `kubernetes-dashboard` accessible from an external ip

### `ClusterIP` to `NodePort`

we need to edit dashboard service and change service "type" from `ClusterIP` to `NodePort`:

on target/host-1:
```
root@k3s-host-1:~# kubectl -n kubernetes-dashboard edit service kubernetes-dashboard
```

the unmodified version looks something like this:
```
# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: v1
kind: Service
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"k8s-app":"kubernetes-dashboard"},"name":"kubernetes-dashboard","namespace":"kubernetes-dashboard"},"spec":{"ports":[{"port":443,"targetPort":8443}],"selector":{"k8s-app":"kubernetes-dashboard"}}}
  creationTimestamp: "2021-08-01T19:20:56Z"
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
  resourceVersion: "5109"
  uid: 9f3e4171-89e0-49ab-8752-8c941e0fefeb
spec:
  clusterIP: 10.43.156.74
  clusterIPs:
  - 10.43.156.74
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - port: 443
    protocol: TCP
    targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```

the modified version looks like this:
```
# Please edit the object below. Lines beginning with a '#' will be ignored, 
# and an empty file will abort the edit. If an error occurs while saving this file will be 
# reopened with the relevant failures. 
# 
apiVersion: v1 
kind: Service 
metadata: 
  annotations: 
    kubectl.kubernetes.io/last-applied-configuration: | 
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"k8s-app":"kubernetes-dashboard"},"name":"kubernetes-dashboard","namespace":"kubernetes-dashboard"},"spec":{"ports":[{"port":443,"targetPort":8443}],"selector":{"k8s-app":"kubernetes-dashboard"}}} 
  creationTimestamp: "2021-08-01T19:20:56Z" 
  labels: 
    k8s-app: kubernetes-dashboard 
  name: kubernetes-dashboard 
  namespace: kubernetes-dashboard 
  resourceVersion: "6024" 
  uid: 9f3e4171-89e0-49ab-8752-8c941e0fefeb 
spec: 
  clusterIP: 10.43.156.74 
  clusterIPs: 
  - 10.43.156.74 
  externalTrafficPolicy: Cluster 
  ipFamilies: 
  - IPv4 
  ipFamilyPolicy: SingleStack 
  ports: 
  - nodePort: 32414 
    port: 443 
    protocol: TCP 
    targetPort: 8443 
  selector: 
    k8s-app: kubernetes-dashboard 
  sessionAffinity: None 
  type: NodePort 
status: 
  loadBalancer: {}
```

### check the new port
on target/host-1:
```
root@k3s-host-1:~# kubectl -n kubernetes-dashboard get services
```

you should see something like this:
```
NAME                        TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)         AGE
dashboard-metrics-scraper   ClusterIP   10.43.204.149   <none>        8000/TCP        26m
kubernetes-dashboard        NodePort    10.43.156.74    <none>        443:32414/TCP   26m
```

on target/host-1:
```
root@k3s-host-1:~# netstat -tuln | grep 32414
```

you should see something like this:
```
tcp        0      0 0.0.0.0:32414           0.0.0.0:*               LISTEN    
```

Now you should be able to connect:

https://[master_node_ip]:[port]

with firefox:

https://192.168.42.251:32414

tick admin token and copy/paste the admin token which we got above - I copied it here again:

```
eyJhbGciOiJSUzI1NiIsImtpZCI6Ill4cDVraUl1eTBOSjBTZERLT3RKbHNmNVE0SUZQWGtRRHlYUnpXQ1ZXcWcifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLTZtcWtsIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI0NjEyZGVkMy0wZTY2LTQ0NWEtYmZkMC00N2MzMTEzNmUxM2EiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6YWRtaW4tdXNlciJ9.pBzxSPm33jTdsRoXCqGySBU84v9M7tV_hcW7NTdZ_Nev9MdvfXqr5s-vaXCYDWP1WbuJq4fmMD8A0J7xV-zagwlWSf4ObUg3q-5U2LFv4bYzR7cVjEQblZSn1G28Ao4n8ddUVou3GEvrzhBHXd35X0iRxAXyVGPRpnyiiAZWiM40KbYeNbdSGnmJXMZOyOrDZBtAA_eYqhkEveSwU6iKPR4YKh6V0eanR1wG1O52XWGkZzmiEe1sfbIcqt4lsP2DjGQ8ECnhDbVv8EumyaiXAM8qS-nL7_f9S527c7EcRD5quhnlRIdrhGJlED0rJYqOZVCLcZEaQcw5IeemTwfKhw
```

![alt text](img/workload-1.png "workload-1")

![alt text](img/nodes-1.png "nodes-1")

![alt text](img/nodes-2.png "nodes-2")