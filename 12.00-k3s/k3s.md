# k3s related tests

## k3s-host

I built `bitbake core-image-minimal-virt-k3s-host` and made sure that in `local.conf` we have `HOST_NAME="k3s-host-1"`.
This should spit out `.wic` image which I flash onto an 64 Gig SD card. I plug the SD card in a `raspberrypi4-64` and boot. 

on target/host-1:
```
root@k3s-host-1:~# kubectl describe nodes
```

you should see something like this:
```
Name:               k3s-host-1                                                                                                                                                                
Roles:              control-plane,master                                                                                                                                                      
Labels:             beta.kubernetes.io/arch=arm64                                                                                                                                             
                    beta.kubernetes.io/instance-type=k3s                                                                                                                                      
                    beta.kubernetes.io/os=linux                                                                                                                                               
                    kubernetes.io/arch=arm64                                                                                                                                                  
                    kubernetes.io/hostname=k3s-host-1                                                                                                                                         
                    kubernetes.io/os=linux                                                                                                                                                    
                    node-role.kubernetes.io/control-plane=true                                                                                                                                
                    node-role.kubernetes.io/master=true                                                                                                                                       
                    node.kubernetes.io/instance-type=k3s                                                                                                                                      
Annotations:        flannel.alpha.coreos.com/backend-data: {"VNI":1,"VtepMAC":"42:c5:ff:aa:7f:40"}                                                                                            
                    flannel.alpha.coreos.com/backend-type: vxlan                                                                                                                              
                    flannel.alpha.coreos.com/kube-subnet-manager: true                                                                                                                        
                    flannel.alpha.coreos.com/public-ip: 192.168.42.248                                                                                                                        
                    k3s.io/hostname: k3s-host-1                                                                                                                                               
                    k3s.io/internal-ip: 192.168.42.248                                                                                                                                        
                    k3s.io/node-args: ["server"]                                                                                                                                              
                    k3s.io/node-config-hash: MLFMUCBMRVINLJJKSG32TOUFWB4CN55GMSNY25AZPESQXZCYRN2A====                                                                                         
                    k3s.io/node-env: {}                                                                                                                                                       
                    node.alpha.kubernetes.io/ttl: 0                                                                                                                                           
                    volumes.kubernetes.io/controller-managed-attach-detach: true                                                                                                              
CreationTimestamp:  Sun, 01 Aug 2021 17:34:17 +0000                                                                                                                                           
Taints:             <none>                                                                                                                                                                    
Unschedulable:      false                                                                                                                                                                     
Lease:                                                                                                                                                                                        
  HolderIdentity:  k3s-host-1                                                                                                                                                                 
  AcquireTime:     <unset>                                                                                                                                                                    
  RenewTime:       Sun, 01 Aug 2021 18:50:30 +0000                                                                                                                                            
Conditions:                                                                                                                                                                                   
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message                                                       
  ----                 ------  -----------------                 ------------------                ------                       -------                                                       
  NetworkUnavailable   False   Sun, 01 Aug 2021 17:34:41 +0000   Sun, 01 Aug 2021 17:34:41 +0000   FlannelIsUp                  Flannel is running on this node
    MemoryPressure       False   Sun, 01 Aug 2021 18:50:09 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available                       
  DiskPressure         False   Sun, 01 Aug 2021 18:50:09 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure                                  
  PIDPressure          False   Sun, 01 Aug 2021 18:50:09 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available                          
  Ready                True    Sun, 01 Aug 2021 18:50:09 +0000   Sun, 01 Aug 2021 17:34:39 +0000   KubeletReady                 kubelet is posting ready status                               
Addresses:                                                                                                                                                                                    
  InternalIP:  192.168.42.248                                                                                                                                                                 
  Hostname:    k3s-host-1                                                                                                                                                                     
Capacity:                                                                                                                                                                                     
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  976568Ki                                                                                                                                                                
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
Allocatable:                                                                                                                                                                                  
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  950005350                                                                                                                                                               
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
System Info:                                                                                                                                                                                  
  Machine ID:                 15484e89530c4203baee0d4d3006da31                                                                                                                                
  System UUID:                15484e89530c4203baee0d4d3006da31                                                                                                                                
  Boot ID:                    4e360e40-2e60-433e-a922-a3d2a87b0312                                                                                                                            
  Kernel Version:             5.10.31-v8                                                                                                                                                      
  OS Image:                   Resy systemd (Reliable Embedded Systems Reference Distro) 3.3+snapshot-f735627e7c5aeb421338db55f3905d74751d4b71 (olivegold)                                     
  Operating System:           linux                                                                                                                                                           
  Architecture:               arm64                                                                                                                                                           
  Container Runtime Version:  containerd://1.5.4-12-g1c13c54ca.m                                                                                                                              
  Kubelet Version:            v1.21.3-k3s1                                                                                                                                                    
  Kube-Proxy Version:         v1.21.3-k3s1                                                                                                                                                    
PodCIDR:                      10.42.0.0/24                                                                                                                                                    
PodCIDRs:                     10.42.0.0/24                                                                                                                                                    
ProviderID:                   k3s://k3s-host-1                                                                                                                                                
Non-terminated Pods:          (5 in total)                                                                                                                                                    
  Namespace                   Name                                       CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age                                                        
  ---------                   ----                                       ------------  ----------  ---------------  -------------  ---                                                        
  kube-system                 coredns-7448499f4d-6j5mw                   100m (2%)     0 (0%)      70Mi (0%)        170Mi (2%)     75m                                                        
  kube-system                 metrics-server-86cbb8457f-hcv8l            0 (0%)        0 (0%)      0 (0%)           0 (0%)         75m                                                        
  kube-system                 local-path-provisioner-5ff76fc89d-l5m7z    0 (0%)        0 (0%)      0 (0%)           0 (0%)         75m                                                        
  kube-system                 svclb-traefik-ws4ps                        0 (0%)        0 (0%)      0 (0%)           0 (0%)         73m                                                        
  kube-system                 traefik-97b44b794-rslsr                    0 (0%)        0 (0%)      0 (0%)           0 (0%)         73m                                                        
Allocated resources:                                                                                                                                                                          
  (Total limits may be over 100 percent, i.e., overcommitted.)                                                                                                                                
  Resource           Requests   Limits                                                                                                                                                        
  --------           --------   ------                                                                                                                                                        
  cpu                100m (2%)  0 (0%)                                                                                                                                                        
  memory             70Mi (0%)  170Mi (2%)                                                                                                                                                    
  ephemeral-storage  0 (0%)     0 (0%)                                                                                                                                                        
Events:              <none>                                                                                                                                
```

on target/host-1:
```
root@k3s-host-1:~# kubectl get pods --all-namespaces
```

you should see something like this:
```
NAMESPACE     NAME                                      READY   STATUS      RESTARTS   AGE
kube-system   coredns-7448499f4d-6j5mw                  1/1     Running     0          78m
kube-system   metrics-server-86cbb8457f-hcv8l           1/1     Running     0          78m
kube-system   local-path-provisioner-5ff76fc89d-l5m7z   1/1     Running     0          78m
kube-system   helm-install-traefik-crd-f4nlx            0/1     Completed   0          78m
kube-system   helm-install-traefik-fchrr                0/1     Completed   1          78m
kube-system   svclb-traefik-ws4ps                       2/2     Running     0          76m
kube-system   traefik-97b44b794-rslsr                   1/1     Running     0          76m

```

on target/host-1:
```
root@k3s-host-1:~# kubectl get services --all-namespaces
```
you should see something like this:
```
NAMESPACE     NAME             TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)                      AGE
default       kubernetes       ClusterIP      10.43.0.1      <none>           443/TCP                      80m
kube-system   kube-dns         ClusterIP      10.43.0.10     <none>           53/UDP,53/TCP,9153/TCP       80m
kube-system   metrics-server   ClusterIP      10.43.195.17   <none>           443/TCP                      80m
kube-system   traefik          LoadBalancer   10.43.64.181   192.168.42.248   80:32266/TCP,443:30112/TCP   77m
```

## k3s-node

I built `bitbake core-image-minimal-virt-k3s-node` and made sure that in `local.conf` we have `HOST_NAME="k3s-node-1"`.
This should spit out `.wic` image which I flash onto an 64 Gig SD card. I plug the SD card in a `raspberrypi4-64` and boot. 

### Add node to cluster

on target/host-1:
```
root@k3s-host-1:~# cat /var/lib/rancher/k3s/server/node-token
```

you should see something like this:
```
K10e2a31bdb063129c654871c813b8ac6096f1c4df4c6b0a27bc17ca0f1db853657::server:f08423a074af246e540beddd68fcf4a5
```

on target/node-1:
```
ping k3s-host-1.res.training
# make sure it replies, otherwise use the IP address of host-1
root@k3s-node-1:~# k3s-agent -t K10e2a31bdb063129c654871c813b8ac6096f1c4df4c6b0a27bc17ca0f1db853657::server:f08423a074af246e540beddd68fcf4a5 -s https://k3s-host-1.res.training:6443
```

you should see something like this:
```
[ 5157.994884] systemd-fstab-generator[540]: Mount point container is not a valid path, ignoring.                                                                                             
[ 5158.021682] systemd-sysv-generator[547]: SysV service '/etc/init.d/conntrack-failover' lacks a native systemd unit file. Automatically generating a unit file for compatibility. Please update package to include a native systemd unit file, in order to make it more safe and robust.                                                                                                  
[ 5158.047715] systemd-sysv-generator[547]: SysV service '/etc/init.d/conntrackd' lacks a native systemd unit file. Automatically generating a unit file for compatibility. Please update package to include a native systemd unit file, in order to make it more safe and robust.                                                                                                          
[ 5158.587854] audit: type=1334 audit(1627844778.653:6): prog-id=9 op=LOAD                                                                                                                    
[ 5158.594657] audit: type=1334 audit(1627844778.653:7): prog-id=10 op=LOAD                                                                                                                   
[ 5158.603529] audit: type=1334 audit(1627844778.653:8): prog-id=7 op=UNLOAD                                                                                                                  
[ 5158.611359] audit: type=1334 audit(1627844778.653:9): prog-id=8 op=UNLOAD                                                                                                                  
[ 5158.619254] audit: type=1334 audit(1627844778.661:10): prog-id=11 op=LOAD                                                                                                                  
[ 5158.627121] audit: type=1334 audit(1627844778.661:11): prog-id=12 op=LOAD                                                                                                                  
[ 5158.635031] audit: type=1334 audit(1627844778.661:12): prog-id=5 op=UNLOAD                                                                                                                 
[ 5158.648258] audit: type=1334 audit(1627844778.661:13): prog-id=6 op=UNLOAD                                                                                                                 
[ 5158.655961] audit: type=1334 audit(1627844778.669:14): prog-id=13 op=LOAD                                                                                                                  
[ 5158.664187] audit: type=1334 audit(1627844778.669:15): prog-id=14 op=LOAD                                                                                                                  
[ 5165.823947] kauditd_printk_skb: 2 callbacks suppressed                                                                                                                                     
[ 5165.823960] audit: type=1325 audit(1627844785.889:18): table=nat family=2 entries=0 op=xt_register pid=569 comm="modprobe"                                                                 
[ 5167.215654] IPVS: [rr] scheduler registered.                                                                                                                                               
[ 5167.229939] IPVS: [wrr] scheduler registered.                                                                                                                                              
[ 5167.249367] IPVS: [sh] scheduler registered.                                                                                                                                               
[ 5172.639795] audit: type=1325 audit(1627844792.704:19): table=nat family=2 entries=5 op=xt_replace pid=598 comm="iptables"                                                                  
[ 5172.675033] audit: type=1325 audit(1627844792.736:20): table=nat family=2 entries=7 op=xt_replace pid=602 comm="iptables"                                                                  
[ 5172.706032] audit: type=1325 audit(1627844792.768:21): table=filter family=2 entries=0 op=xt_register pid=608 comm="modprobe"                                                              
[ 5172.720569] audit: type=1325 audit(1627844792.784:22): table=filter family=2 entries=4 op=xt_replace pid=607 comm="iptables"                                                               
[ 5172.783120] audit: type=1325 audit(1627844792.844:23): table=filter family=2 entries=6 op=xt_replace pid=613 comm="iptables"                                                               
[ 5172.825209] audit: type=1325 audit(1627844792.888:24): table=filter family=2 entries=7 op=xt_replace pid=616 comm="iptables"                                                               
[ 5172.841120] audit: type=1325 audit(1627844792.904:25): table=filter family=2 entries=8 op=xt_replace pid=618 comm="iptables"                                                               
[ 5172.857447] audit: type=1325 audit(1627844792.920:26): table=filter family=2 entries=9 op=xt_replace pid=620 comm="iptables"                                                               
[ 5172.869309] audit: type=1325 audit(1627844792.928:27): table=nat family=2 entries=8 op=xt_replace pid=621 comm="iptables"                                                                  
[ 5172.881238] audit: type=1325 audit(1627844792.936:28): table=nat family=2 entries=10 op=xt_replace pid=622 comm="iptables"                                                                 
Created symlink /etc/systemd/system/multi-user.target.wants/k3s-agent.service -> /lib/systemd/system/k3s-agent.service.                                                                       
[ 5174.088687] systemd-sysv-generator[690]: SysV service '/etc/init.d/conntrack-failover' lacks a native systemd unit file. Automatically generating a unit file for compatibility. Please update package to include a native systemd unit file, in order to make it more safe and robust.                                                                                                  
[ 5174.090487] systemd-fstab-generator[683]: Mount point container is not a valid path, ignoring.                                                                                             
[ 5174.114347] systemd-sysv-generator[690]: SysV service '/etc/init.d/conntrackd' lacks a native systemd unit file. Automatically generating a unit file for compatibility. Please update package to include a native systemd unit file, in order to make it more safe and robust.                                                                                                          
root@k3s-node-1:~# [ 5183.297117] kauditd_printk_skb: 97 callbacks suppressed                                                                                                                 
[ 5183.297128] audit: type=1325 audit(1627844803.360:126): table=filter family=2 entries=52 op=xt_replace pid=846 comm="iptables"                                                             
[ 5183.314238] audit: type=1325 audit(1627844803.368:127): table=filter family=2 entries=53 op=xt_replace pid=847 comm="iptables"                                                             
[ 5183.345163] audit: type=1325 audit(1627844803.408:128): table=filter family=2 entries=52 op=xt_replace pid=851 comm="iptables"                                                             
[ 5183.356846] audit: type=1325 audit(1627844803.416:129): table=filter family=2 entries=53 op=xt_replace pid=853 comm="iptables"                                                             
[ 5183.387769] audit: type=1325 audit(1627844803.452:130): table=filter family=2 entries=52 op=xt_replace pid=857 comm="iptables"                                                             
[ 5183.399431] audit: type=1325 audit(1627844803.456:131): table=filter family=2 entries=53 op=xt_replace pid=858 comm="iptables"                                                             
[ 5183.577067] audit: type=1325 audit(1627844803.640:132): table=filter family=2 entries=52 op=xt_replace pid=876 comm="iptables-restor"                                                      
[ 5190.374319] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready                                                                                                                        
[ 5190.391922] cni0: port 1(vethfeed995a) entered blocking state                                                                                                                              
[ 5190.398225] cni0: port 1(vethfeed995a) entered disabled state                                                               
...

```

### Check on host

```
root@k3s-host-1:~# kubectl describe nodes
```

you should see something like this:
```
Name:               k3s-host-1                                                                                                                                                                
Roles:              control-plane,master                                                                                                                                                      
Labels:             beta.kubernetes.io/arch=arm64                                                                                                                                             
                    beta.kubernetes.io/instance-type=k3s                                                                                                                                      
                    beta.kubernetes.io/os=linux                                                                                                                                               
                    kubernetes.io/arch=arm64                                                                                                                                                  
                    kubernetes.io/hostname=k3s-host-1                                                                                                                                         
                    kubernetes.io/os=linux                                                                                                                                                    
                    node-role.kubernetes.io/control-plane=true                                                                                                                                
                    node-role.kubernetes.io/master=true                                                                                                                                       
                    node.kubernetes.io/instance-type=k3s                                                                                                                                      
Annotations:        flannel.alpha.coreos.com/backend-data: {"VNI":1,"VtepMAC":"42:c5:ff:aa:7f:40"}                                                                                            
                    flannel.alpha.coreos.com/backend-type: vxlan                                                                                                                              
                    flannel.alpha.coreos.com/kube-subnet-manager: true                                                                                                                        
                    flannel.alpha.coreos.com/public-ip: 192.168.42.248                                                                                                                        
                    k3s.io/hostname: k3s-host-1                                                                                                                                               
                    k3s.io/internal-ip: 192.168.42.248                                                                                                                                        
                    k3s.io/node-args: ["server"]                                                                                                                                              
                    k3s.io/node-config-hash: MLFMUCBMRVINLJJKSG32TOUFWB4CN55GMSNY25AZPESQXZCYRN2A====                                                                                         
                    k3s.io/node-env: {}                                                                                                                                                       
                    node.alpha.kubernetes.io/ttl: 0                                                                                                                                           
                    volumes.kubernetes.io/controller-managed-attach-detach: true                                                                                                              
CreationTimestamp:  Sun, 01 Aug 2021 17:34:17 +0000                                                                                                                                           
Taints:             <none>                                                                                                                                                                    
Unschedulable:      false                                                                                                                                                                     
Lease:                                                                                                                                                                                        
  HolderIdentity:  k3s-host-1                                                                                                                                                                 
  AcquireTime:     <unset>                                                                                                                                                                    
  RenewTime:       Sun, 01 Aug 2021 19:09:07 +0000                                                                                                                                            
Conditions:                                                                                                                                                                                   
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message                                                       
  ----                 ------  -----------------                 ------------------                ------                       -------                                                       
  NetworkUnavailable   False   Sun, 01 Aug 2021 17:34:41 +0000   Sun, 01 Aug 2021 17:34:41 +0000   FlannelIsUp                  Flannel is running on this node                               
  MemoryPressure       False   Sun, 01 Aug 2021 19:05:36 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available                       
  DiskPressure         False   Sun, 01 Aug 2021 19:05:36 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure                                  
  PIDPressure          False   Sun, 01 Aug 2021 19:05:36 +0000   Sun, 01 Aug 2021 17:34:16 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available                
  Ready                True    Sun, 01 Aug 2021 19:05:36 +0000   Sun, 01 Aug 2021 17:34:39 +0000   KubeletReady                 kubelet is posting ready status                               
Addresses:                                                                                                                                                                                    
  InternalIP:  192.168.42.248                                                                                                                                                                 
  Hostname:    k3s-host-1                                                                                                                                                                     
Capacity:                                                                                                                                                                                     
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  976568Ki                                                                                                                                                                
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
Allocatable:                                                                                                                                                                                  
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  950005350                                                                                                                                                               
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
System Info:                                                                                                                                                                                  
  Machine ID:                 15484e89530c4203baee0d4d3006da31                                                                                                                                
  System UUID:                15484e89530c4203baee0d4d3006da31                                                                                                                                
  Boot ID:                    4e360e40-2e60-433e-a922-a3d2a87b0312                                                                                                                            
  Kernel Version:             5.10.31-v8                                                                                                                                                      
  OS Image:                   Resy systemd (Reliable Embedded Systems Reference Distro) 3.3+snapshot-f735627e7c5aeb421338db55f3905d74751d4b71 (olivegold)                                     
  Operating System:           linux                                                                                                                                                           
  Architecture:               arm64                                                                                                                                                           
  Container Runtime Version:  containerd://1.5.4-12-g1c13c54ca.m                                                                                                                              
  Kubelet Version:            v1.21.3-k3s1                                                                                                                                                    
  Kube-Proxy Version:         v1.21.3-k3s1                                                                                                                                                    
PodCIDR:                      10.42.0.0/24                                                                                                                                                    
PodCIDRs:                     10.42.0.0/24                                                                                                                                                    
ProviderID:                   k3s://k3s-host-1                                                                                                                                                
Non-terminated Pods:          (5 in total)                                                                                                                                                    
  Namespace                   Name                                       CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age                                                        
  ---------                   ----                                       ------------  ----------  ---------------  -------------  ---                                                        
  kube-system                 coredns-7448499f4d-6j5mw                   100m (2%)     0 (0%)      70Mi (0%)        170Mi (2%)     94m                                                        
  kube-system                 metrics-server-86cbb8457f-hcv8l            0 (0%)        0 (0%)      0 (0%)           0 (0%)         94m                                                        
  kube-system                 local-path-provisioner-5ff76fc89d-l5m7z    0 (0%)        0 (0%)      0 (0%)           0 (0%)         94m                                                        
  kube-system                 svclb-traefik-ws4ps                        0 (0%)        0 (0%)      0 (0%)           0 (0%)         92m                                                        
  kube-system                 traefik-97b44b794-rslsr                    0 (0%)        0 (0%)      0 (0%)           0 (0%)         92m                                                        
Allocated resources:                                                                                                                                                                          
  (Total limits may be over 100 percent, i.e., overcommitted.)                                                                                                                                
  Resource           Requests   Limits                                                                                                                                                        
  --------           --------   ------                                                                                                                                                        
  cpu                100m (2%)  0 (0%)                                                                                                                                                        
  memory             70Mi (0%)  170Mi (2%)                                                                                                                                                    
  ephemeral-storage  0 (0%)     0 (0%)                                                                                                                                                        
Events:              <none>                                                                                                                                                                   
                                                                                                                                                                                              
                                                                                                                                                                                              
Name:               k3s-node-1                                                
Roles:              <none>                                                                                                                                                                    
Labels:             beta.kubernetes.io/arch=arm64                                                                                                                                             
                    beta.kubernetes.io/instance-type=k3s                                                                                                                                      
                    beta.kubernetes.io/os=linux                                                                                                                                               
                    kubernetes.io/arch=arm64                                                                                                                                                  
                    kubernetes.io/hostname=k3s-node-1                                                                                                                                         
                    kubernetes.io/os=linux                                                                                                                                                    
                    node.kubernetes.io/instance-type=k3s                                                                                                                                      
Annotations:        flannel.alpha.coreos.com/backend-data: {"VNI":1,"VtepMAC":"fe:82:db:3a:ab:9b"}                                                                                            
                    flannel.alpha.coreos.com/backend-type: vxlan                                                                                                                              
                    flannel.alpha.coreos.com/kube-subnet-manager: true                                                                                                                        
                    flannel.alpha.coreos.com/public-ip: 192.168.42.251                                                                                                                        
                    k3s.io/hostname: k3s-node-1                                                                                                                                               
                    k3s.io/internal-ip: 192.168.42.251                                                                                                                                        
                    k3s.io/node-args: ["agent"]                                                                                                                                               
                    k3s.io/node-config-hash: 62MR7AJYILHGDTVOIFLBUA7LBNZRECSDNLCIGXK54OGTOEAS76LA====                                                                                         
                    k3s.io/node-env: {"K3S_TOKEN":"********","K3S_URL":"https://k3s-host-1.res.training:6443"}                                                                                
                    node.alpha.kubernetes.io/ttl: 0                                                                                                                                           
                    volumes.kubernetes.io/controller-managed-attach-detach: true                                                                                                              
CreationTimestamp:  Sun, 01 Aug 2021 19:06:32 +0000                                                                                                                                           
Taints:             <none>                                                                                                                                                                    
Unschedulable:      false                                                                                                                                                                     
Lease:                                                                                                                                                                                        
  HolderIdentity:  k3s-node-1                                                                                                                                                                 
  AcquireTime:     <unset>                                                                                                                                                                    
  RenewTime:       Sun, 01 Aug 2021 19:09:18 +0000                                                                                                                                            
Conditions:                                                                                                                                                                                   
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message                                                       
  ----                 ------  -----------------                 ------------------                ------                       -------                                                       
  NetworkUnavailable   False   Sun, 01 Aug 2021 19:06:34 +0000   Sun, 01 Aug 2021 19:06:34 +0000   FlannelIsUp                  Flannel is running on this node                               
  MemoryPressure       False   Sun, 01 Aug 2021 19:07:03 +0000   Sun, 01 Aug 2021 19:06:32 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available                       
  DiskPressure         False   Sun, 01 Aug 2021 19:07:03 +0000   Sun, 01 Aug 2021 19:06:32 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure                                  
  PIDPressure          False   Sun, 01 Aug 2021 19:07:03 +0000   Sun, 01 Aug 2021 19:06:32 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available                          
  Ready                True    Sun, 01 Aug 2021 19:07:03 +0000   Sun, 01 Aug 2021 19:06:42 +0000   KubeletReady                 kubelet is posting ready status                               
Addresses:                                                                                                                                                                                    
  InternalIP:  192.168.42.251                                                                                                                                                                 
  Hostname:    k3s-node-1                                                                                                                                                                     
Capacity:                                                                                                                                                                                     
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  976568Ki                                                                                                                                                                
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                                                                                                                                                     
Allocatable:                                                                                                                                                                                  
  cpu:                4                                                                                                                                                                       
  ephemeral-storage:  950005350                                                                                                                                                               
  memory:             7998408Ki                                                                                                                                                               
  pods:               110                                       
  System Info:                                                                                                                                                                                  
  Machine ID:                 205cef95869541298fe5a8f6a64a872e                                                                                                                                
  System UUID:                205cef95869541298fe5a8f6a64a872e                                                                                                                                
  Boot ID:                    0751c1d6-8ee5-483c-af96-20454f863446                                                                                                                            
  Kernel Version:             5.10.31-v8                                                                                                                                                      
  OS Image:                   Resy systemd (Reliable Embedded Systems Reference Distro) 3.3+snapshot-f735627e7c5aeb421338db55f3905d74751d4b71 (olivegold)                                     
  Operating System:           linux                                                                                                                                                           
  Architecture:               arm64                                                                                                                                                           
  Container Runtime Version:  containerd://1.5.4-12-g1c13c54ca.m                                                                                                                              
  Kubelet Version:            v1.21.3-k3s1                                                                                                                                                    
  Kube-Proxy Version:         v1.21.3-k3s1                                                                                                                                                    
PodCIDR:                      10.42.1.0/24                                                                                                                                                    
PodCIDRs:                     10.42.1.0/24                                                                                                                                                    
ProviderID:                   k3s://k3s-node-1                                                                                                                                                
Non-terminated Pods:          (1 in total)                                                                                                                                                    
  Namespace                   Name                   CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age                                                                            
  ---------                   ----                   ------------  ----------  ---------------  -------------  ---                                                                            
  kube-system                 svclb-traefik-stl7h    0 (0%)        0 (0%)      0 (0%)           0 (0%)         2m36s                                                                          
Allocated resources:                                                                                                                                                                          
  (Total limits may be over 100 percent, i.e., overcommitted.)                                                                                                                                
  Resource           Requests  Limits                                                                                                                                                         
  --------           --------  ------                                                                                                                                                         
  cpu                0 (0%)    0 (0%)                                                                                                                                                         
  memory             0 (0%)    0 (0%)                                                                                                                                                         
  ephemeral-storage  0 (0%)    0 (0%)                                                                                                                                                         
Events:                                                                                                                                                                                       
  Type     Reason                   Age    From        Message                                                                                                                                
  ----     ------                   ----   ----        -------                                                                                                                                
  Normal   Starting                 2m47s  kubelet     Starting kubelet.                                                                                                                      
  Warning  InvalidDiskCapacity      2m47s  kubelet     invalid capacity 0 on image filesystem                                                                                                 
  Normal   NodeHasSufficientMemory  2m47s  kubelet     Node k3s-node-1 status is now: NodeHasSufficientMemory                                                                                 
  Normal   NodeHasNoDiskPressure    2m47s  kubelet     Node k3s-node-1 status is now: NodeHasNoDiskPressure                                                                                   
  Normal   NodeHasSufficientPID     2m47s  kubelet     Node k3s-node-1 status is now: NodeHasSufficientPID                                                                                    
  Normal   NodeAllocatableEnforced  2m46s  kubelet     Updated Node Allocatable limit across pods                                                                                             
  Normal   Starting                 2m44s  kube-proxy  Starting kube-proxy.                                                                                                                   
  Normal   NodeReady                2m37s  kubelet     Node k3s-node-1 status is now: NodeReady            
```

on target/host-1:
```
root@k3s-host-1:~# kubectl get pods --all-namespaces
```

you should see something like this:
```
kube-system   coredns-7448499f4d-6j5mw                  1/1     Running     0          96m
kube-system   metrics-server-86cbb8457f-hcv8l           1/1     Running     0          96m
kube-system   local-path-provisioner-5ff76fc89d-l5m7z   1/1     Running     0          96m
kube-system   helm-install-traefik-crd-f4nlx            0/1     Completed   0          96m
kube-system   helm-install-traefik-fchrr                0/1     Completed   1          96m
kube-system   svclb-traefik-ws4ps                       2/2     Running     0          94m
kube-system   traefik-97b44b794-rslsr                   1/1     Running     0          94m
kube-system   svclb-traefik-stl7h                       2/2     Running     0          4m50s
```

on target/host-1:
```
root@k3s-host-1:~# kubectl get services --all-namespaces
```
you should see something like this:
```
AMESPACE     NAME             TYPE           CLUSTER-IP     EXTERNAL-IP                     PORT(S)                      AGE
default       kubernetes       ClusterIP      10.43.0.1      <none>                          443/TCP                      97m
kube-system   kube-dns         ClusterIP      10.43.0.10     <none>                          53/UDP,53/TCP,9153/TCP       97m
kube-system   metrics-server   ClusterIP      10.43.195.17   <none>                          443/TCP                      97m
kube-system   traefik          LoadBalancer   10.43.64.181   192.168.42.248,192.168.42.251   80:32266/TCP,443:30112/TCP   94m
```

