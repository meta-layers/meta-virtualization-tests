# docker (classic) build

## some theory

In a container we don't need a kernel. As a matter of fact we could put a single application
and it's dependencies in a container. This is called "micro architecture".

`poky` provides an `image-container.bbclass`[1] which helps us to build a root file system suitable
for being included in a container image.

[1] https://git.yoctoproject.org/cgit/cgit.cgi/poky/tree/meta/classes/image-container.bbclass

## rootfs built with `poky`

Let's try to use `poky` plus a custom layer to create an x86-64 (qemu) based root file system suitable for 
being included in a `Dockerfile`.

## checkout layers

In my setup I use `/workdir/sources` as the place where I'll keep the `meta-layers` and `/workdir/build` for 
the actual build.

### get poky
on the host:
```
cd /workdir/sources
git clone git://git.yoctoproject.org/poky poky-master
```

### get meta-container-examples
on the host:
```
cd /workdir/sources
git clone https://gitlab.com/meta-layers/meta-container-ex-compact.git meta-container-ex-compact-docker-only -b docker-only 
```

## checkout support scripts

### get manifests/support scripts
on the host:
```
cd /workdir/sources
git clone https://github.com/RobertBerger/manifests
```
## set some symlinks
on the host:
```
cd /workdir
ln -sf sources/manifests/resy-cooker.sh resy-cooker.sh
ln -sf sources/manifests/resy-poky-container.sh resy-poky-container.sh
ln -sf sources/manifests/killall_bitbake.sh killall_bitbake.sh
```

## What to build

Let's say we wanted to build a root file system suitable for being included in a docker container 
for x86-64. In the rootfs we want `lighttpd` and `busybox`.

## `meta-container-ex-compact`
I created `meta-container-ex-compact` from various other layers of mine in order to cook up relatively 
simple examples here.

If we inspect `meta-container-ex-compact` on the `docker-only` branch this is what we'll see:

on the host:
```
tree /workdir/sources/meta-container-ex-compact-docker-only
```

you should see something like that:
```
/workdir/sources/meta-container-ex-compact-docker-only
├── classes
│   └── image-manifestinfo.bbclass
├── conf
│   ├── distro
│   │   ├── include
│   │   │   └── common-all-distro.inc
│   │   └── resy-container.conf
│   ├── layer.conf
│   └── machine
│       └── container-x86-64.conf
├── COPYING.MIT
├── README
├── recipes-core
│   └── images
│       ├── app-container-image.bb
│       ├── app-container-image-lighttpd.bb
│       ├── app-container-image-lighttpd-common.inc
│       └── common-img.inc
├── scripts
│   ├── config.sh
│   ├── patch-modified.sh
│   ├── push-upstream.sh
│   ├── remote-get-modified-files.sh
│   ├── remote-get-untracked-files.sh
│   ├── remote-scp-modified-files.sh
│   ├── remote-scp-untracked-files.sh
│   ├── remove-modified-and-untracked.sh
│   ├── scp-modified-files.sh
│   ├── scp-untracked-files.sh
│   └── untar-untracked.sh
└── template-container-x86-64-ex-compact-docker-only
    ├── 01-local.conf.glibc.plus-lic-info
    ├── 02-local.conf.glibc.less-metadata
    ├── 03-local.conf.musl.less-metadata
    ├── bblayers.conf.sample
    ├── conf-notes.txt
    ├── local.conf.sample
    └── site.conf

9 directories, 29 files
```

Let's have a look at these directories and files and see what they doing and which can be ignored.
### classes (dir)

```
/workdir/sources/meta-container-ex-compact-docker-only
├── classes
│   └── image-manifestinfo.bbclass
```

This contains `image-manifestinfo.bbclass`[2] which is actually completely irrelevant for containers.

[2] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/classes/image-manifestinfo.bbclass

The only thing it does is to write image-maniufest information to `/etc/image-manifest`.
It's there because my default files include it, but you can igore it.

### conf (dir)

```
├── conf
│   ├── distro
│   │   ├── include
│   │   │   └── common-all-distro.inc
│   │   └── resy-container.conf
│   ├── layer.conf
│   └── machine
│       └── container-x86-64.conf
```

#### conf/distro

```
├── conf
│   ├── distro
│   │   ├── include
│   │   │   └── common-all-distro.inc
│   │   └── resy-container.conf
```

`resy-container.conf`[3] defines the `resy-container` distro, which is a wrapper around the `poky` distro
and removes a few things which are not needed for the container root file system.
It includes `common-all-distro.inc` [4] which adds a few variables to the buildinfo. 
I find this information convenient in my containers/targets but it is non essential for containers.

[3] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/conf/distro/resy-container.conf

[4] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/conf/distro/include/common-all-distro.inc

#### conf/machine

```
├── conf
│   └── machine
│       └── container-x86-64.conf
```

`container-x86-64.conf` [5] is the machine configuration. For simplicity reasons it is based on `qemux86-64.conf` 
which comes with `openembedded-core`/`poky` and replaces the default kernel provider with `linux-dummy`.

[5] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/conf/machine/container-x86-64.conf

### default files

```
/workdir/sources/meta-container-ex-compact
├── COPYING.MIT
├── README
```

I left them as they were composed from the `bitbake-layers` tool - sorry.

### image recipe

```
├── recipes-core
│   └── images
│       ├── app-container-image.bb
│       ├── app-container-image-lighttpd.bb
│       ├── app-container-image-lighttpd-common.inc
│       └── common-img.inc
```

This is how I strctured it:

`app-container-image-lighttpd.bb` [6] includes `app-container-image.bb` [7] and `app-container-image-lighttpd-common.inc` [8].

`app-container-image.bb` [8] includes `common-img.inc` [9] which includes again what I think we already included before ;)

Let's have a look at those files:

`app-container-image-lighttpd.bb` [6] is the top level file and the one which will build our root file system. As you can see it just includes a couple of files.

`app-container-image.bb` [7] is my "standard" include template for container images. You could just use it as it is.

`app-container-image-lighttpd-common.inc` [8] contains the packages which we would like to install into our root file system. Note that it is struictured like that and called `common` because it is common e.g. between this examaple here
and oci images.

`common-img.inc` [9] just does something I find convenient, but it is non essential for containes.

[6] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/recipes-core/images/app-container-image-lighttpd.bb

[7] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/recipes-core/images/app-container-image.bb

[8] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/recipes-core/images/app-container-image-lighttpd-common.inc

[9] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/recipes-core/images/common-img.inc

### custom standard scripts

```
├── scripts
│   ├── config.sh
│   ├── patch-modified.sh
│   ├── push-upstream.sh
│   ├── remote-get-modified-files.sh
│   ├── remote-get-untracked-files.sh
│   ├── remote-scp-modified-files.sh
│   ├── remote-scp-untracked-files.sh
│   ├── remove-modified-and-untracked.sh
│   ├── scp-modified-files.sh
│   ├── scp-untracked-files.sh
│   └── untar-untracked.sh
```

You can totally ignore them. I use them internally for my workflow.

### template

```
└── template-container-x86-64-ex-compact-docker-only
    ├── 01-local.conf.glibc.plus-lic-info
    ├── 02-local.conf.glibc.less-metadata
    ├── 03-local.conf.musl.less-metadata
    ├── bblayers.conf.sample
    ├── conf-notes.txt
    ├── local.conf.sample
    └── site.conf
```

This is my custom template configuration directory. The underlying mechanism is desribed here [10]

[10] https://docs.yoctoproject.org/dev-manual/common-tasks.html#creating-a-custom-template-configuration-directory

We checked out `/workdir/sources/manifests` above and created some symlinks, which are used, among other things, 
to call the template configuration.

#### bblayers.conf

`bblayers.conf.conf` [11] is the template for `bblayers.conf`. As you can see only the layers in `poky` and `meta-container-ex-compact` are used.

[11] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/template-container-x86-64-ex-compact-docker-only/bblayers.conf.sample

#### conf-notes.txt

`conf-notes.txt` [12] just displays possible build targets like `app-container-image-lighttpd`.

[12] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/template-container-x86-64-ex-compact-docker-only/conf-notes.txt

#### local.conf

`local.conf.sample` [13] is out top level config file. Especially important are those variables:

[13] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/template-container-x86-64-ex-compact-docker-only/local.conf.sample

this is our custom machine config:

```
MACHINE ??= "container-x86-64"
```

this is our custom distro:
```
DISTRO ?= "resy-container"
```

### site.conf

`site.conf` [14] is customized for my local setup. You might give it a try with it, ignore it, or adjust it to your setup. Please note that my wrapper scripts will automatically copy it over once.

[14] https://gitlab.com/meta-layers/meta-container-ex-compact/-/blob/docker-only/template-container-x86-64-ex-compact-master/site.conf

## build container and setup

The assumption is, that you have a docker engine installed on your PC for this to work.

Please note that for this to work out of the box (without you hacking my scripts) you need to have the directly
structure as mentioned above. 

I build my stuff preperably in a build container. This is how I set it up:

on the host:
```
cd /workdir

./resy-poky-container.sh container-x86-64-ex-compact-docker-only

```

This should download a docker container and bring you to a shell inside the container, 
where the yocto build environment is already setup for you.

You should see somerhing along those lines:

```
+ docker pull reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18
2021-05-07-master-local-gcc-9-gui-icecc-ub18: Pulling from reliableembeddedsystems/poky-container
Digest: sha256:3f83f549071bfb1bdf1609b0bb977d4ba494ac5cdaa262e641a09a7d1827cb4a
Status: Image is up to date for reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18
docker.io/reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18
+ set +x
 -- interactive mode + MACHINE --
+ export BUILDDIR=/workdir/build/container-x86-64-ex-compact-docker-only
+ BUILDDIR=/workdir/build/container-x86-64-ex-compact-docker-only
+ /workdir/killall_bitbake.sh
student  22737 22703  0 19:12 pts/1    00:00:00 /bin/bash /workdir/killall_bitbake.sh
student  22739 22737  0 19:12 pts/1    00:00:00 grep bitbake
student  22737 22703  0 19:12 pts/1    00:00:00 /bin/bash /workdir/killall_bitbake.sh
student  22744 22737  0 19:12 pts/1    00:00:00 grep bitbake
rm -f /workdir/build/container-x86-64-ex-compact-docker-only/hashserve.sock
+ docker run --name poky_container --rm -i -t --add-host mirror:192.168.42.1 --net=host --env BUILD_ALL=no -v /home/student/projects:/projects -v /opt:/nfs -v /workdir:/workdir -v /workdir:/
workdir reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18 /bin/bash -c 'source /workdir/resy-cooker.sh container-x86-64-ex-compact-docker-only && /bin/bash'
 --workdir=/workdir 
/workdir ~
MACHINE or MACHINE-sw-variant: container-x86-64-ex-compact-docker-only
initial SITE_CONF=../../sources/meta-resy/template-common/site.conf.sample
TEMPLATECONF: ../meta-container-ex-compact-docker-only/template-container-x86-64-ex-compact-docker-only
/workdir/build
source ../sources/poky-master/oe-init-build-env container-x86-64-ex-compact-docker-only
You had no conf/local.conf file. This configuration file has therefore been
created for you with some default values. You may wish to edit it to, for
example, select a different MACHINE (target hardware). See conf/local.conf
for more information as common configuration options are commented.

You had no conf/bblayers.conf file. This configuration file has therefore been
created for you with some default values. To add additional metadata layers
into your configuration please add entries to conf/bblayers.conf.

The Yocto Project has extensive documentation about OE including a reference
manual which can be found at:
    http://yoctoproject.org/documentation

For more information about OpenEmbedded see their website:
    http://www.openembedded.org/


### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are: 
    app-container-image-lighttpd

You can also run generated qemu images with a command like 'runqemu qemux86'
conf
├── bblayers.conf   
├── local.conf
├── site.conf
└── templateconf.cfg

0 directories, 4 files
~
--> $#: 1
pokyuser    61    21  0 17:12 pts/0    00:00:00 /bin/bash /workdir/killall_bitbake.sh
pokyuser    63    61  0 17:12 pts/0    00:00:00 grep bitbake
pokyuser    61    21  0 17:12 pts/0    00:00:00 /bin/bash /workdir/killall_bitbake.sh
pokyuser    68    61  0 17:12 pts/0    00:00:00 grep bitbake
rm -f /workdir/build/container-x86-64-ex-compact-docker-only/hashserve.sock
to ./resy-poky-container.sh:
 -- non-interactive mode --
 add the image you want to build to the command line ./resy-poky-container.sh <MACHINE> <image>
 -- interactive mode --
   enter it with - ./resy-poky-container.sh <no param>
   bitbake <image>  
   source /workdir/resy-cooker.sh <MACHINE>
pokyuser@e450-13:~$ 
```
### Build.

You see above that `pokyuser` is your current user, which indicates we are in a docker container:

In the container:
```
bitbake app-container-image-lighttpd
```

And you should see something along those lines:

```
NOTE: Started PRServer with DBfile: /workdir/build/container-x86-64-ex-compact-docker-only/cache/prserv.sqlite3, IP: 127.0.0.1, PORT: 32781, PID: 88
Loading cache: 100% |                                                                                                                                                         | ETA:  --:--:--
Loaded 0 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:01:16
Parsing of 823 .bb files complete (0 cached, 823 parsed). 1453 targets, 187 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies

Build Configuration:
BB_VERSION           = "1.50.0"
BUILD_SYS            = "x86_64-linux"
NATIVELSBSTRING      = "ubuntu-18.04"
TARGET_SYS           = "x86_64-resy-linux"
MACHINE              = "container-x86-64"
DISTRO               = "resy-container"
DISTRO_VERSION       = "3.3+snapshot-c21419b1fee3098298a77efdcca322fa665dea54"
TUNE_FEATURES        = "m64 core2"
TARGET_FPU           = ""
meta
meta-poky
meta-yocto-bsp       = "master:c21419b1fee3098298a77efdcca322fa665dea54"
meta-container-ex-compact-docker-only = "docker-only:44057325b850d90398cd154e2cda4b2039cf8906"

Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:01
Sstate summary: Wanted 580 Local 578 Network 0 Missed 2 Current 0 (99% match, 0% complete)
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 1726 tasks of which 1572 didn't need to be rerun and all succeeded.
NOTE: Writing buildhistory
NOTE: Writing buildhistory took: 1 seconds
```

Now we have the root file system, which is suitable for inclusion in a `Dockerfile`.

## Docker

### scripts to build app-container-lighttpd docker container

```
cd /workdir
mkdir app-container-x86-64 && cd app-container-x86-64
git clone https://gitlab.com/app-container/app-container-lighttpd.git -b x86-64
```

### Dockerfile

As you can see the `Dockerfile`[14] looks like this:

```
FROM scratch
ADD app-container-image-lighttpd-container-x86-64.tar.bz2 /
```

[14] https://gitlab.com/app-container/app-container-lighttpd/-/blob/x86-64/dockerfile/Dockerfile

### helper scripts

One script, which you might need to adjust, since I might change it in my setup to build other things
is `/workdir/build/container-x86-64-ex-compact-master/tmp/deploy/images/container-x86-64/docker_build.sh` [15]

[15] https://gitlab.com/app-container/app-container-lighttpd/-/blob/x86-64/local_scripts/docker_build.sh

It should look like this:

```
source ../container-name.sh
set -x
rm -f ../dockerfile/${IMAGE_NAME}.tar.bz2
cp /workdir/build/container${ARCH}-ex-compact-docker-only/tmp/deploy/images/container${ARCH}/${IMAGE_NAME}.tar.bz2 ../dockerfile/${IMAGE_NAME}.tar.bz2
docker build --rm=true -t reslocal/${CONTAINER_NAME} ../dockerfile/
rm -f ../dockerfile/${IMAGE_NAME}.tar.bz2
set +x
```

### build app-container-lighttpd docker container

The assumption is, that you have a docker engine installed on your PC for this to work.

On the host:
```
cd /workdir/app-container-x86-64/app-container-lighttpd/local_scripts
./docker_build.sh
```

You should see someting along those lines:

```
++ rm -f ../dockerfile/app-container-image-lighttpd-container-x86-64.tar.bz2
++ cp /workdir/build/container-x86-64-ex-compact-master/tmp/deploy/images/container-x86-64/app-container-image-lighttpd-container-x86-64.tar.bz2 ../dockerfile/app-container-image-lighttpd-container-x86-64.tar.bz2
++ docker build --rm=true -t reslocal/app-container-lighttpd-x86-64 ../dockerfile/
Sending build context to Docker daemon  3.712MB
Step 1/2 : FROM scratch
 ---> 
Step 2/2 : ADD app-container-image-lighttpd-container-x86-64.tar.bz2 /
 ---> Using cache
 ---> 45c8420d87f5
Successfully built 45c8420d87f5
Successfully tagged reslocal/app-container-lighttpd-x86-64:latest
++ rm -f ../dockerfile/app-container-image-lighttpd-container-x86-64.tar.bz2
++ set +x
```

### Run the container

On the host:
```
cd /workdir/app-container-x86-64/app-container-lighttpd/local_scripts
./docker_run.sh reslocal/app-container-lighttpd-x86-64
```

You should see something like this:
```
+ ID_TO_KILL=$(docker ps -a -q  --filter ancestor=reslocal/app-container-lighttpd-x86-64)
+ docker ps -a
CONTAINER ID        IMAGE                                                                                 COMMAND                  CREATED             STATUS              PORTS               NAMES
ed1b5f0365e3        reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18   "/usr/bin/dumb-init …"   24 minutes ago      Up 24 minutes                           poky_container
+ docker stop 
"docker stop" requires at least 1 argument.
See 'docker stop --help'.

Usage:  docker stop [OPTIONS] CONTAINER [CONTAINER...]

Stop one or more running containers
+ docker rm -f 
"docker rm" requires at least 1 argument.
See 'docker rm --help'.

Usage:  docker rm [OPTIONS] CONTAINER [CONTAINER...]

Remove one or more containers
+ docker ps -a
CONTAINER ID        IMAGE                                                                                 COMMAND                  CREATED             STATUS              PORTS               NAMES
ed1b5f0365e3        reliableembeddedsystems/poky-container:2021-05-07-master-local-gcc-9-gui-icecc-ub18   "/usr/bin/dumb-init …"   24 minutes ago      Up 24 minutes                           poky_container
++ docker run -p 8079:80 --entrypoint=/bin/sh reslocal/app-container-lighttpd-x86-64 -c '/etc/init.d/lighttpd restart && tail -f /var/log/access.log'
Restarting Lighttpd Web Server: no /usr/sbin/lighttpd found; none killed
lighttpd.
```

### Test

You shoule be able to connect from a web browser to your container `http://<ip where container runs>:8079`

The webbrowser should say "It works" and where the container runs you should see logs like these

```
192.168.42.52 192.168.42.113:8079 - [02/May/2021:12:38:00 +0000] "GET / HTTP/1.1" 200 45 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
192.168.42.52 192.168.42.113:8079 - [02/May/2021:12:38:01 +0000] "GET /favicon.ico HTTP/1.1" 404 341 "http://192.168.42.113:8079/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
192.168.42.52 192.168.42.113:8079 - [02/May/2021:12:38:12 +0000] "GET / HTTP/1.1" 200 45 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
192.168.42.52 192.168.42.113:8079 - [02/May/2021:12:38:12 +0000] "GET /favicon.ico HTTP/1.1" 404 341 "http://192.168.42.113:8079/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"
```


