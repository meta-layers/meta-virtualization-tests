# docker/docker-compose related tests

## docker clean up

on target:
```
root@multi-v7-ml:~# docker ps -a
```
you might see something like this:
```
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS                         PORTS                    NAMES
bec8af50845c   b309878813fe            "/bin/sh -c 'apt-get…"   32 minutes ago   Exited (100) 31 minutes ago                             lucid_jackson
9dd68f80b19f   busybox                 "/bin/sh"                3 hours ago      Exited (0) About an hour ago                            bold_jang
25f1bd9187e5   busybox                 "/bin/sh -c '/bin/ht…"   5 hours ago      Exited (137) 5 hours ago                                dreamy_lamport
09a9530030bd   busybox                 "/bin/sh"                5 hours ago      Exited (130) 5 hours ago                                agitated_rosalind
641f31e8aba8   busybox                 "/bin/sh"                6 hours ago      Exited (130) 6 hours ago                                clever_chaum
5aa029f2aeae   busybox                 "/bin/sh"                6 hours ago      Exited (0) 6 hours ago                                  thirsty_ganguly
03e2ed71d39d   telegraf:1.17.2         "/entrypoint.sh tele…"   19 hours ago     Exited (127) 6 hours ago                                telegraf
6eac833e0fbd   influxdb:1.8.4          "/entrypoint.sh infl…"   19 hours ago     Up 59 minutes                  0.0.0.0:8086->8086/tcp   influxdb
a09e0c456563   grafana/grafana:7.4.0   "/run.sh"                19 hours ago     Exited (127) 6 hours ago       0.0.0.0:3000->3000/tcp   grafana
root@multi-v7-ml:~# 
```

stop all containers:
```
root@multi-v7-ml:~# docker stop $(docker ps -a -q)
```
you might see something like this:
```
bec8af50845c
9dd68f80b19f
25f1bd9187e5
09a9530030bd
641f31e8aba8
5aa029f2aeae
03e2ed71d39d
[ 3683.284337] br-df16a15eb838: port 1(veth747afbb) entered disabled state
[ 3683.291176] veth9b54ce7: renamed from eth0
[ 3683.664967] br-df16a15eb838: port 1(veth747afbb) entered disabled state
[ 3683.678655] device veth747afbb left promiscuous mode
[ 3683.683694] br-df16a15eb838: port 1(veth747afbb) entered disabled state
6eac833e0fbd
a09e0c456563
```
remove the running containers:
```
root@multi-v7-ml:~# docker rm -f $(docker ps --no-trunc -a -q)
```
you might see something like this:
```
bec8af50845c98280e8379c8f9a2db99fad83ed766f13ed7fba37bb36c72601e
9dd68f80b19f1311787641b612660996db7465033cdf59743f6b55e9db87edef
25f1bd9187e5b587bc6f678a518fd6e7526f2267a5376d8504b073169b4b0539
09a9530030bdae9e78441f9dcddc7354f4e410506055203bc5ca5d2502b4ad61
641f31e8aba8aaf17d8521f97f85443fc92fa398c6342192cd3579a6671f6459
5aa029f2aeae2f08cb23fa2555c2722f3daa604387b9f06b0a8d99bc5343913d
03e2ed71d39d83ee2fb8171387b5dd60d9ac08188d8490bbef275923296c02f4
6eac833e0fbdb41cb088d161b947fe773d32026a5db47b7b9ac6d0af0eac0d83
a09e0c456563604c4d0750c45a780135b974083b5babe17200a2f4212232fbbd
root@multi-v7-ml:~# 
```

confirm that they are gone:
```
root@multi-v7-ml:~# docker ps -a
```
you should see something like this:
```
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
root@multi-v7-ml:~# 
```

Please note that the images should be still here
```
root@multi-v7-ml:~# docker images
```
you might see something like this:
```
REPOSITORY        TAG             IMAGE ID       CREATED          SIZE
<none>            <none>          b309878813fe   39 minutes ago   56.5MB
influxdb          1.8.4           b981467ac4ae   22 hours ago     264MB
telegraf          1.17.2          2d9c338bde64   22 hours ago     230MB
debian            bullseye-slim   b9559b448e44   39 hours ago     56.5MB
grafana/grafana   7.4.0           80f8781ad990   6 days ago       164MB
busybox           latest          d3fde6046eed   8 days ago       962kB
root@multi-v7-ml:~# 
```
(optional) if you want to remove the images as well:
```
root@multi-v7-ml:~# docker rmi $(docker images -a -q)
```
you might see something like this:
```
Deleted: sha256:b309878813fefb071f56f5d30a4c3eb9156391b1f5c051e9d3747a9a7b29759f
Untagged: influxdb:1.8.4
Untagged: influxdb@sha256:4b309fde55b0c06dbdf24670bc7905d7b4ce8e3b698452fc798b0433937eae72
Deleted: sha256:b981467ac4ae2e2898af181253117226acacf5235d51bbd0942434847568d9d3
Deleted: sha256:fb122697ec1cf8e23c7d1bfe7cf70128f98ef4954ed3e2016b1a407d92fb1022
Deleted: sha256:078d918d2d11daf3514ddb0b3349d0af0a60ae939cde0a9b630659d28493660a
Deleted: sha256:5917bc1e7e3ede6e56ea045dfef81d44c3c2e7a5a7bc67ce831fb04318627615
Deleted: sha256:5f4599591a9f5c75b4f8d6249efe7f5a53ba02d04d3bc6f8b61638b2f5cb0a0e
Deleted: sha256:bcb2488d239edfa915f658afa8413db5683ca68ec1d9c93406c11a82fef638da
Deleted: sha256:7f45a838094cd0cd3acf35b70af86adfa7de9b171a49264bf1a09440e519c389
Deleted: sha256:301c6cb9affc448a15795044c8aa4c3f33c4cb4b03684339384dfcb931563daf
Deleted: sha256:ed066485a37e7e3c5bed6164c5d238f2199f2fd3f834e90ff4a6d6a39c865b7e
...
Untagged: busybox:latest
Untagged: busybox@sha256:e1488cb900233d035575f0a7787448cb1fa93bed0ccc0d4efc1963d7d72a8f17
Deleted: sha256:d3fde6046eed872276c4b8bdfedf5f8bdee3f39878eb2304949ab69beea43b74
Deleted: sha256:28637ec3468483be47bba0fc625dad4980698ac1efd7ed7a88f32e4d56e20402
root@multi-v7-ml:~# 
```

## docker-compose example

on target:
```
cd 
git clone https://gitlab.com/meta-layers/meta-virtualization-tests.git
cd meta-virtualization-tests
git config --global
git config pull.rebase false
```
you should see something like this:
```
Cloning into 'meta-virtualization-tests'...
remote: Enumerating objects: 82, done.
remote: Counting objects: 100% (82/82), done.
remote: Compressing objects: 100% (68/68), done.
remote: Total 82 (delta 10), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (82/82), 261.04 KiB | 587.00 KiB/s, done.
Resolving deltas: 100% (10/10), done.
```
```
root@multi-v7-ml:~# cd 04-docker-compose/04.01-tig/
```
erase whatever we did before:
```
root@multi-v7-ml:~/meta-virtualization-tests/04-docker-compose/04.01-tig# ./01_erase-project.sh 
```
you you see something like this:
```
+ rm -rf /home/root/meta-virtualization-tests/04-docker-compose/04.01-tig/projects/tig/
+ set +x
```
create the project structure/config:
```
root@multi-v7-ml:~/meta-virtualization-tests/04-docker-compose/04.01-tig# ./02_create-stuff.sh 
```
you you see something like this:
```
+ mkdir -p /home/root/meta-virtualization-tests/04-docker-compose/04.01-tig/projects/tig/data/influxdb
+ mkdir -p /home/root/meta-virtualization-tests/04-docker-compose/04.01-tig/projects/tig/data/grafana
+ mkdir -p /home/root/meta-virtualization-tests/04-docker-compose/04.01-tig/projects/tig/log/grafana
+ mkdir -p /home/root/meta-virtualization-tests/04-docker-compose/04.01-tig/projects/tig/data/mqtt/
+ mkdir -p /home/root/meta-virtualization-tests/04-docker-compose/04.01-tig/projects/tig/log/mqtt/
+ chmod 777 -R /home/root/meta-virtualization-tests/04-docker-compose/04.01-tig/projects/tig/
+ set +x
```
create the `.env` for `docker-compose`:
```
root@multi-v7-ml:~/meta-virtualization-tests/04-docker-compose/04.01-tig# ./03_create-env.sh 
```
you should see something like this:
```
+ rm -f .env
+ pwd
+ echo 'HERE=/home/root/meta-virtualization-tests/04-docker-compose/04.01-tig'
+ ln -sf .env env
+ cat env
HERE=/home/root/meta-virtualization-tests/04-docker-compose/04.01-tig
+ set +x
```
`docker-compose up`:
```
root@multi-v7-ml:~/meta-virtualization-tests/04-docker-compose/04.01-tig# ./04_docker-compose-up.sh 
```
+ docker-compose up
you should see something like this:
```
[ 1970.173999] Initializing XFRM netlink socket
Building with native build. Learn about native build in Compose here: https://docs.docker.com/go/compose-native-build/
Creating network "0401-tig_default" with the default driver
Pulling influxdb (influxdb:1.8.4)...
1.8.4: Pulling from library/influxdb
5275c778c803: Extracting [======>                                            ]  5.538MB/42.12MB
aae60b5ed65c: Download complete
a99de29c3c44: Download complete
266519cf240c: Download complete
566dbce2ab06: Downloading [========================>                          ]  30.52MB/61.06MB
ce0ef9b950cd: Download complete
97b8013e08b5: Download complete
206c65c8a4e2: Download complete
...
```
In case the container images are not already here, it will download influxdb, telegraf, grafana.
```
...
+ docker-compose up
Building with native build. Learn about native build in Compose here: https://docs.docker.com/go/compose-native-build/
Creating network "0401-tig_default" with the default driver
Creating grafana ... 
Creating influxdb ... 
[ 3343.836514] br-f110e1ce2f0c: port 1(veth3e4af2f) entered blocking state
[ 3343.843260] br-f110e1ce2f0c: port 1(veth3e4af2f) entered disabled state
[ 3343.850211] device veth3e4af2f entered promiscuous mode
[ 3344.124774] br-f110e1ce2f0c: port 2(vethdc1726b) entered blocking state
[ 3344.131479] br-f110e1ce2f0c: port 2(vethdc1726b) entered disabled state
[ 3344.138385] device vethdc1726b entered promiscuous mode
[ 3344.144877] br-f110e1ce2f0c: port 2(vethdc1726b) entered blocking state
[ 3344.151577] br-f110e1ce2f0c: port 2(vethdc1726b) entered forwarding state
[ 3344.159363] br-f110e1ce2f0c: port 2(vethdc1726b) entered disabled state
[ 3347.836344] eth0: renamed from veth932169d
[ 3347.892927] IPv6: ADDRCONF(NETDEV_CHANGE): veth3e4af2f: link becomes ready
[ 3347.899954] br-f110e1ce2f0c: port 1(veth3e4af2f) entered blocking state
[ 3347.906693] br-f110e1ce2f0c: port 1(veth3e4af2f) entered forwarding state
[ 3347.913871] IPv6: ADDRCONF(NETDEV_CHANGE): br-f110e1ce2f0c: link becomes ready
[ 3348.187167] eth0: renamed from veth6847c44
[ 3348.223265] IPv6: ADDRCONF(NETDEV_CHANGE): vethdc1726b: link becomes ready
Creating grafana  ... done
Creating influxdb ... done
Creating telegraf ... done
Attaching to grafana, influxdb, telegraf
grafana     | t=2021-02-10T19:10:10+0000 lvl=warn msg="[Deprecated] the configuration setting 'login_maximum_inactive_lifetime_days' is deprecated, please use 'login_maximum_inactive_lifetime_duration' instead" logger=settings
grafana     | t=2021-02-10T19:10:10+0000 lvl=warn msg="[Deprecated] the configuration setting 'login_maximum_lifetime_days' is deprecated, please use 'login_maximum_lifetime_duration' instead" logger=settings
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Starting Grafana" logger=server version=7.4.0 commit=c2203b9859 branch=HEAD compiled=2021-02-04T11:11:00+0000
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config loaded from" logger=settings file=/usr/share/grafana/conf/defaults.ini
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config loaded from" logger=settings file=/etc/grafana/grafana.ini
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.data=/var/lib/grafana"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.logs=/var/log/grafana"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.plugins=/var/lib/grafana/plugins"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.provisioning=/etc/grafana/provisioning"
influxdb    | ts=2021-02-10T19:10:10.479966Z lvl=info msg="InfluxDB starting" log_id=0SFLStBl000 version=1.8.4 branch=1.8 commit=bc8ec4384eed25436d31045f974bf39f3310fa3c
influxdb    | ts=2021-02-10T19:10:10.480090Z lvl=info msg="Go runtime" log_id=0SFLStBl000 version=go1.13.8 maxprocs=4
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.log.mode=console"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_DATA=/var/lib/grafana"
influxdb    | ts=2021-02-10T19:10:10.599522Z lvl=info msg="Using data dir" log_id=0SFLStBl000 service=store path=/var/lib/influxdb/data
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_LOGS=/var/log/grafana"
influxdb    | ts=2021-02-10T19:10:10.603224Z lvl=info msg="Compaction settings" log_id=0SFLStBl000 service=store max_concurrent_compactions=2 throughput_bytes_per_second=50331648 throughput_bytes_per_second_burst=50331648
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_PLUGINS=/var/lib/grafana/plugins"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_PROVISIONING=/etc/grafana/provisioning"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Path Home" logger=settings path=/usr/share/grafana
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Path Data" logger=settings path=/var/lib/grafana
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Path Logs" logger=settings path=/var/log/grafana
influxdb    | ts=2021-02-10T19:10:10.605013Z lvl=info msg="Open store (start)" log_id=0SFLStBl000 service=store trace_id=0SFLStfl000 op_name=tsdb_open op_event=start
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Path Plugins" logger=settings path=/var/lib/grafana/plugins
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Path Provisioning" logger=settings path=/etc/grafana/provisioning
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="App mode production" logger=settings
influxdb    | ts=2021-02-10T19:10:10.607060Z lvl=info msg="Open store (end)" log_id=0SFLStBl000 service=store trace_id=0SFLStfl000 op_name=tsdb_open op_event=end op_elapsed=2.077ms
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Connecting to DB" logger=sqlstore dbtype=sqlite3
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Creating SQLite database file" logger=sqlstore path=/var/lib/grafana/grafana.db
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Starting DB migrations" logger=migrator
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create migration_log table"
influxdb    | ts=2021-02-10T19:10:10.607880Z lvl=info msg="Opened service" log_id=0SFLStBl000 service=subscriber
influxdb    | ts=2021-02-10T19:10:10.607975Z lvl=info msg="Starting monitor service" log_id=0SFLStBl000 service=monitor
influxdb    | ts=2021-02-10T19:10:10.608723Z lvl=info msg="Registered diagnostics client" log_id=0SFLStBl000 service=monitor name=build
influxdb    | ts=2021-02-10T19:10:10.608839Z lvl=info msg="Registered diagnostics client" log_id=0SFLStBl000 service=monitor name=runtime
influxdb    | ts=2021-02-10T19:10:10.608903Z lvl=info msg="Registered diagnostics client" log_id=0SFLStBl000 service=monitor name=network
influxdb    | ts=2021-02-10T19:10:10.608986Z lvl=info msg="Registered diagnostics client" log_id=0SFLStBl000 service=monitor name=system
influxdb    | ts=2021-02-10T19:10:10.609418Z lvl=info msg="Starting precreation service" log_id=0SFLStBl000 service=sha
influxdb    | ts=2021-02-10T19:10:10.610138Z lvl=info msg="Starting snapshot service" log_id=0SFLStBl000 service=snapshot
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create user table"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="add unique index user.login"
influxdb    | ts=2021-02-10T19:10:10.611093Z lvl=info msg="Storing statistics" log_id=0SFLStBl000 service=monitor db_instance=_internal db_rp=monitor interval=10s
influxdb    | ts=2021-02-10T19:10:10.613678Z lvl=info msg="Starting continuous query service" log_id=0SFLStBl000 service=continuous_querier
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="add unique index user.email"
influxdb    | ts=2021-02-10T19:10:10.614686Z lvl=info msg="Starting HTTP service" log_id=0SFLStBl000 service=httpd authentication=false
influxdb    | ts=2021-02-10T19:10:10.615139Z lvl=info msg="opened HTTP access log" log_id=0SFLStBl000 service=httpd path=stderr
influxdb    | ts=2021-02-10T19:10:10.615821Z lvl=info msg="Listening on HTTP" log_id=0SFLStBl000 service=httpd addr=[::]:8086 https=false
influxdb    | ts=2021-02-10T19:10:10.616392Z lvl=info msg="Starting retention policy enforcement service" log_id=0SFLStBl000 service=retention check_interval=30m
influxdb    | ts=2021-02-10T19:10:10.617222Z lvl=info msg="Listening for signals" log_id=0SFLStBl000
influxdb    | ts=2021-02-10T19:10:10.618039Z lvl=info msg="Sending usage statistics to usage.influxdata.com" log_id=0SFLStBl000
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="drop index UQE_user_login - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="drop index UQE_user_email - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Rename table user to user_v1 - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create user table v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index UQE_user_login - v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index UQE_user_email - v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="copy data_source v1 to v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Drop old table user_v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Add column help_flags1 to user table"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Update user table charset"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Add last_seen_at column to user"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Add missing user data"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Add is_disabled column to user"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Add index user.login/user.email"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create temp user table v1-7"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_email - v1-7"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_org_id - v1-7"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_code - v1-7"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_status - v1-7"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Update temp_user table charset"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="drop index IDX_temp_user_email - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="drop index IDX_temp_user_org_id - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="drop index IDX_temp_user_code - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="drop index IDX_temp_user_status - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="Rename table temp_user to temp_user_tmp_qwerty - v1"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create temp_user v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_email - v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_org_id - v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_code - v2"
grafana     | t=2021-02-10T19:10:10+0000 lvl=info msg="Executing migration" logger=migrator id="create index IDX_temp_user_status - v2"
...
grafana     | t=2021-02-10T19:10:16+0000 lvl=info msg="Executing migration" logger=migrator id="create cache_data table"
grafana     | t=2021-02-10T19:10:16+0000 lvl=info msg="Executing migration" logger=migrator id="add unique index cache_data.cache_key"
grafana     | t=2021-02-10T19:10:16+0000 lvl=info msg="Executing migration" logger=migrator id="create short_url table v1"
grafana     | t=2021-02-10T19:10:16+0000 lvl=info msg="Executing migration" logger=migrator id="add index short_url.org_id-uid"
telegraf    | 2021-02-10T19:10:17Z I! Starting Telegraf 1.17.2
telegraf    | 2021-02-10T19:10:17Z I! Using config file: /etc/telegraf/telegraf.conf
telegraf    | 2021-02-10T19:10:17Z I! Loaded inputs: cpu disk docker mem mqtt_consumer net swap system
telegraf    | 2021-02-10T19:10:17Z I! Loaded aggregators: 
telegraf    | 2021-02-10T19:10:17Z I! Loaded processors: 
telegraf    | 2021-02-10T19:10:17Z I! Loaded outputs: influxdb (2x)
telegraf    | 2021-02-10T19:10:17Z I! Tags enabled: host=multi-v7-ml
telegraf    | 2021-02-10T19:10:17Z I! [agent] Config: Interval:10s, Quiet:false, Hostname:"multi-v7-ml", Flush Interval:10s
influxdb    | ts=2021-02-10T19:10:17.301534Z lvl=info msg="Executing query" log_id=0SFLStBl000 service=query query="CREATE DATABASE vm_metrics"
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:17 +0000] "POST /query HTTP/1.1" 200 57 "-" "Telegraf/1.17.2 Go/1.15.5" 9c8280fb-6bd3-11eb-8001-0242ac140003 19367
influxdb    | ts=2021-02-10T19:10:17.340792Z lvl=info msg="Executing query" log_id=0SFLStBl000 service=query query="CREATE DATABASE docker_metrics"
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:17 +0000] "POST /query HTTP/1.1" 200 57 "-" "Telegraf/1.17.2 Go/1.15.5" 9c889065-6bd3-11eb-8002-0242ac140003 14578
grafana     | t=2021-02-10T19:10:17+0000 lvl=info msg="Created default admin" logger=sqlstore user=admin
grafana     | t=2021-02-10T19:10:17+0000 lvl=info msg="Created default organization" logger=sqlstore
grafana     | t=2021-02-10T19:10:17+0000 lvl=info msg="Starting plugin search" logger=plugins
grafana     | t=2021-02-10T19:10:21+0000 lvl=info msg="Registering plugin" logger=plugins id=input
grafana     | t=2021-02-10T19:10:21+0000 lvl=info msg="HTTP Server Listen" logger=http.server address=[::]:3000 protocol=http subUrl= socket=
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:27 +0000] "POST /write?db=vm_metrics HTTP/1.1" 204 0 "-" "Telegraf/1.17.2 Go/1.15.5" a2cecf40-6bd3-11eb-8003-0242ac140003 279979
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:27 +0000] "POST /write?db=docker_metrics HTTP/1.1" 204 0 "-" "Telegraf/1.17.2 Go/1.15.5" a2ced47c-6bd3-11eb-8004-0242ac140003 516068
grafana     | t=2021-02-10T19:10:37+0000 lvl=eror msg="Failed to look up user based on cookie" logger=context error="user token not found"
grafana     | t=2021-02-10T19:10:37+0000 lvl=info msg="Request Completed" logger=context userId=0 orgId=0 uname= method=GET path=/ status=302 remote_addr=192.168.42.52 time_ms=8 size=29 referer=
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:37 +0000] "POST /write?db=vm_metrics HTTP/1.1" 204 0 "-" "Telegraf/1.17.2 Go/1.15.5" a8c4bcd3-6bd3-11eb-8005-0242ac140003 37086
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:37 +0000] "POST /write?db=docker_metrics HTTP/1.1" 204 0 "-" "Telegraf/1.17.2 Go/1.15.5" a8c4f9da-6bd3-11eb-8006-0242ac140003 35522
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:47 +0000] "POST /write?db=vm_metrics HTTP/1.1" 204 0 "-" "Telegraf/1.17.2 Go/1.15.5" aebad16a-6bd3-11eb-8007-0242ac140003 20485
influxdb    | [httpd] 172.20.0.1 - - [10/Feb/2021:19:10:47 +0000] "POST /write?db=docker_metrics HTTP/1.1" 204 0 "-" "Telegraf/1.17.2 Go/1.15.5" aebad164-6bd3-11eb-8008-0242ac140003 48825
```

Check that influxdb is available outside by accessing e.g. from a host via a web brower:
```
http://<target-ip-address>:8086/query
```

On the browser you should see:

![alt text](img/influxdb-query-1.png "error:	missing required parameter")

Check that grafana is available outside by accessing e.g. from a host via a web brower:

```
http://<target-ip-address>:3000
```

On the browser you should see:

![alt text](img/grafana-1.png "grafana login")

Log in: 

username: admin

password: admin

Afterwards you can either skip or add a prover username/password, which you should remember.

After logging in you should see a scren like this:

![alt text](img/grafana-2.png "grafana welcome")

Proceed to create a new data source:

![alt text](img/grafana-3-data-source-1.png "grafana data source")

Add data source:

![alt text](img/grafana-4-add-data-source.png "grafana add data source")

Pick influxdb:

![alt text](img/grafana-5-select-influxdb.png "pick influxdb")

Connect to the database - make sure you pick the right ip address of your target device:

![alt text](img/grafana-6-influxdb.png "pick influxdb")

Click "Save & Test" and make sure you see "Data source is working" like here:

![alt text](img/grafana-7-data-source-is-working.png "data source is working")

Download a `dashbaord.json` to the host machine which runs your web browser.

e.g.
```
cd /tmp
wget https://gitlab.com/meta-layers/meta-virtualization-tests/-/raw/master/04-docker-compose/04.01-tig/dashboards/system_vm/system_vm-1612128206100.json
```
you should see something like this:
```
--2021-02-10 22:04:19--  https://gitlab.com/meta-layers/meta-virtualization-tests/-/raw/master/04-docker-compose/04.01-tig/dashboards/system_vm/system_vm-1612128206100.json
Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [text/plain]
Saving to: ‘system_vm-1612128206100.json’

system_vm-1612128206100.json                        [ <=>                                                                                                  ]  16.67K  --.-KB/s    in 0.001s  

2021-02-10 22:04:19 (32.0 MB/s) - ‘system_vm-1612128206100.json’ saved [17071]

rber@t460s-2:/tmp$ 

```

Get ready to import the dashboard:

![alt text](img/grafana-8-import-dashboard.png "import dashboard")

Upload JSON:

![alt text](img/grafana-9-upload-json.png "upload JSON")

Upload the dashboard we got before:

![alt text](img/grafana-10-upload-json.png "upload JSON")

Import the dashboard - click Import:

![alt text](img/grafana-11-import.png "import")

Now you should have a dashbaord:

![alt text](img/grafana-12-dashboard.png "dashboard")
