#!/bin/sh
HERE=$(pwd)
if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo
fi

set -x
${SUDO} rm -rf ${HERE}/projects
set +x
