#!/bin/sh
set -x
rm -f .env
echo "HERE=$(pwd)" > .env
echo "INFLUXDB_USERNAME=admin" >> .env
echo "INFLUXDB_PASSWORD=admin" >> .env
ln -sf .env env
cat env
set +x
