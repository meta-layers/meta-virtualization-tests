#!/bin/sh

HERE=$(pwd)

if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo
fi

set -x
mkdir -p ${HERE}/projects/tig/data/influxdb/
mkdir -p ${HERE}/projects/tig/data/grafana/
mkdir -p ${HERE}/projects/tig/log/grafana/
mkdir -p ${HERE}/projects/tig/data/mqtt/
mkdir -p ${HERE}/projects/tig/log/mqtt/
mkdir -p ${HERE}/projects/tig/data/chronograf/ 
cp -r conf ${HERE}/projects/
${SUDO} chown -R 1883:1883 ${HERE}/projects/tig/data/mqtt/
${SUDO} chown -R 1883:1883 ${HERE}/projects/tig/log/mqtt/
${SUDO} chmod 777 -R ${HERE}/projects/tig/
set +x
