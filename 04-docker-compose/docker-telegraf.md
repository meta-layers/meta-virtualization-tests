# telegraf is crashing

```
docker-compose up telegraf
```
you might see something like this:
```
Building with native build. Learn about native build in Compose here: https://docs.docker.com/go/compose-native-build/
influxdb is up-to-date
Starting telegraf ... done
Attaching to telegraf
telegraf    | 2021-03-07T09:54:01Z I! Starting Telegraf 1.17.3
telegraf    | 2021-03-07T09:54:01Z I! Using config file: /etc/telegraf/telegraf.conf
telegraf    | 2021-03-07T09:54:01Z I! Loaded inputs: cpu disk docker mem mqtt_consumer net swap system
telegraf    | 2021-03-07T09:54:01Z I! Loaded aggregators: 
telegraf    | 2021-03-07T09:54:01Z I! Loaded processors: 
telegraf    | 2021-03-07T09:54:01Z I! Loaded outputs: influxdb (2x)
telegraf    | 2021-03-07T09:54:01Z I! Tags enabled: host=2755d374f0f7
telegraf    | 2021-03-07T09:54:01Z I! [agent] Config: Interval:10s, Quiet:false, Hostname:"2755d374f0f7", Flush Interval:10s
telegraf    | 2021-03-07T09:54:01Z I! [inputs.mqtt_consumer] Connected [tcp://mqttbrk1.res.training:1883]
telegraf    | panic: runtime error: slice bounds out of range [:4] with capacity 0
telegraf    | 
telegraf    | goroutine 116 [running]:
telegraf    | github.com/influxdata/telegraf/plugins/inputs/docker.parseContainerStats(0x4366340, 0x283d988, 0x40562e0, 0x4056cc0, 0x43920c0, 0x40, 0x1, 0x406f410, 0x5)
telegraf    | 	/go/src/github.com/influxdata/telegraf/plugins/inputs/docker/docker.go:677 +0x1ed8
telegraf    | github.com/influxdata/telegraf/plugins/inputs/docker.(*Docker).gatherContainerInspect(0x3d58400, 0x43920c0, 0x40, 0x4056c60, 0x1, 0x4, 0x40fa560, 0x17, 0x3c30780, 0x47, ...)
telegraf    | 	/go/src/github.com/influxdata/telegraf/plugins/inputs/docker/docker.go:568 +0x35c
telegraf    | github.com/influxdata/telegraf/plugins/inputs/docker.(*Docker).gatherContainer(0x3d58400, 0x43920c0, 0x40, 0x4056c60, 0x1, 0x4, 0x40fa560, 0x17, 0x3c30780, 0x47, ...)
telegraf    | 	/go/src/github.com/influxdata/telegraf/plugins/inputs/docker/docker.go:495 +0x6f0
telegraf    | github.com/influxdata/telegraf/plugins/inputs/docker.(*Docker).Gather.func1(0x407e0a0, 0x3d58400, 0x283d988, 0x40562e0, 0x43920c0, 0x40, 0x4056c60, 0x1, 0x4, 0x40fa560, ...)
telegraf    | 	/go/src/github.com/influxdata/telegraf/plugins/inputs/docker/docker.go:217 +0x68
telegraf    | created by github.com/influxdata/telegraf/plugins/inputs/docker.(*Docker).Gather
telegraf    | 	/go/src/github.com/influxdata/telegraf/plugins/inputs/docker/docker.go:215 +0x3b4
telegraf exited with code 2


```


# get mqtt to run

First you need to make sure that the entry in `docker-compose` and the `mosquitto.conf` are working.

## start only mosquitto mqtt broker (for tests)

on target:
```
docker-compose run mqtt        
```
you might see something like this from the container:
```
1614249506: mosquitto version 2.0.7 starting
1614249506: Config loaded from /mosquitto/config/mosquitto.conf.
1614249506: Opening ipv4 listen socket on port 1883.
1614249506: Opening ipv6 listen socket on port 1883.
1614249506: mosquitto version 2.0.7 running
1614249507: New connection from 192.168.42.61:51688 on port 1883.
1614249507: New connection from 192.168.42.63:49406 on port 1883.
1614249507: New client connected from 192.168.42.61:51688 as DVES_195994 (p2, c1, k30, u'DVES_USER').
1614249507: Will message specified (7 bytes) (r1, q1).
1614249507: 	tele/line/office/ac/LWT
1614249507: Sending CONNACK to DVES_195994 (0, 0)
1614249507: New client connected from 192.168.42.63:49406 as DVES_193801 (p2, c1, k30, u'DVES_USER').
1614249507: Will message specified (7 bytes) (r1, q1).
1614249507: 	tele/line/2/LWT
1614249507: Sending CONNACK to DVES_193801 (0, 0)
1614249507: New connection from 192.168.42.65:53576 on port 1883.
1614249507: New client connected from 192.168.42.65:53576 as DVES_B64335 (p2, c1, k30, u'DVES_USER').
1614249507: Will message specified (7 bytes) (r1, q1).
1614249507: 	tele/line/office/racktr/LWT
1614249507: Sending CONNACK to DVES_B64335 (0, 0)
1614249507: Received PUBLISH from DVES_193801 (d0, q0, r1, m0, 'tele/line/2/LWT', ... (6 bytes))
1614249507: Received PUBLISH from DVES_195994 (d0, q0, r1, m0, 'tele/line/office/ac/LWT', ... (6 bytes))
1614249507: Received PUBLISH from DVES_B64335 (d0, q0, r1, m0, 'tele/line/office/racktr/LWT', ... (6 bytes))
1614249507: Received PUBLISH from DVES_195994 (d0, q0, r0, m0, 'cmnd/line/office/ac/POWER', ... (0 bytes))
1614249507: Received SUBSCRIBE from DVES_195994
1614249507: 	cmnd/line/office/ac/# (QoS 0)
1614249507: DVES_195994 0 cmnd/line/office/ac/#
1614249507: Sending SUBACK to DVES_195994
1614249507: Received PUBLISH from DVES_193801 (d0, q0, r0, m0, 'cmnd/line/2/POWER', ... (0 bytes))
1614249507: Received PUBLISH from DVES_B64335 (d0, q0, r0, m0, 'cmnd/line/office/racktr/POWER', ... (0 bytes))
1614249507: Received SUBSCRIBE from DVES_B64335
1614249507: 	cmnd/line/office/racktr/# (QoS 0)
1614249507: DVES_B64335 0 cmnd/line/office/racktr/#
1614249507: Sending SUBACK to DVES_B64335
1614249507: Received SUBSCRIBE from DVES_195994
1614249507: 	cmnd/tasmotas/# (QoS 0)
1614249507: DVES_195994 0 cmnd/tasmotas/#
1614249507: Sending SUBACK to DVES_195994
1614249507: Received SUBSCRIBE from DVES_195994
1614249507: 	cmnd/DVES_195994_fb/# (QoS 0)
1614249507: DVES_195994 0 cmnd/DVES_195994_fb/#
1614249507: Sending SUBACK to DVES_195994
```

If you see the above you aleady have sensors up and running and subscribed to the mqtt broker.

## connect with a shell to running container to test publish/subscribe

on target:
```
docker ps -a
```
you might see something like this:
```
CONTAINER ID   IMAGE                     COMMAND                  CREATED          STATUS          PORTS      NAMES
15388e68f9e8   eclipse-mosquitto:2.0.7   "/bin/sh -c '/usr/sb…"   11 minutes ago   Up 11 minutes   1883/tcp   0401-tig_mqtt_run_6f70922068d2
```

Note the name from above

```
docker exec -it 15388e68f9e8 /bin/sh                        
```
This brings us in the container:
```
/ $ 
```

Let's see if `extra_hosts` is set up correctly in `docker-compose.yml`:

in container:
```
/ $ ping mqttbrk1.res.training
```
you might see something like this:
```
PING mqttbrk1.res.training (192.168.42.71): 56 data bytes
64 bytes from 192.168.42.71: seq=0 ttl=42 time=0.208 ms
64 bytes from 192.168.42.71: seq=1 ttl=42 time=0.168 ms
^C
```

Let's check pub/sub:

in container:

subscribe to new topic:
```
/ $ mosquitto_sub -h mqttbrk1.res.training -p 1883 -t news &
```

publish to new topic:
```
/ $ mosquitto_pub -h mqttbrk1.res.training -p 1883 -t news -m "Hello Reliable Embedded Systems"
```
you should see something like this:
```
Hello Reliable Embedded Systems
```
and we can also have a look at the logs:
```
/ $ cat /mosquitto/log/mosquitto.log | grep news
```
you should see something like this:
```
1614252192: 	news (QoS 0)
1614252192: auto-55FCA126-793C-B8AD-C484-43564215D1C1 0 news
1614252239: Received PUBLISH from auto-4B615186-4364-8872-0205-C842A2F25B54 (d0, q0, r0, m0, 'news', ... (31 bytes))
1614252239: Sending PUBLISH to auto-55FCA126-793C-B8AD-C484-43564215D1C1 (d0, q0, r0, m0, 'news', ... (31 bytes))
```

We can get some meta information from the server:

```
/ $ mosquitto_sub -v -t \$SYS/#
```
you should see something like this:
```
$SYS/broker/version mosquitto version 2.0.7
$SYS/broker/uptime 704 seconds
$SYS/broker/load/messages/received/1min 19.73
$SYS/broker/load/messages/received/5min 18.49
$SYS/broker/load/messages/received/15min 11.56
$SYS/broker/load/messages/sent/1min 12.50
$SYS/broker/load/messages/sent/5min 10.78
$SYS/broker/load/messages/sent/15min 6.71
$SYS/broker/load/publish/received/1min 7.23
$SYS/broker/load/publish/received/5min 7.61
$SYS/broker/load/publish/received/15min 4.69
$SYS/broker/load/bytes/received/1min 2116.06
$SYS/broker/load/bytes/received/5min 2177.68
$SYS/broker/load/bytes/received/15min 1335.58
$SYS/broker/load/bytes/sent/1min 25.51
$SYS/broker/load/bytes/sent/5min 28.25
$SYS/broker/load/bytes/sent/15min 19.06
$SYS/broker/load/sockets/5min 0.28
$SYS/broker/load/connections/5min 0.28
$SYS/broker/messages/received 258
$SYS/broker/messages/sent 150
$SYS/broker/store/messages/bytes 234
$SYS/broker/publish/messages/received 103
$SYS/broker/publish/bytes/received 25011
$SYS/broker/bytes/received 29231
$SYS/broker/bytes/sent 442
```

## Let's try with real sensors

### The Sensor

I've got a sensor here:

![alt text](img/tasmota-1.png "tasmota-1")

Configure -> Configure MQTT:

![alt text](img/tasmota-2.png "tasmota-2")

MQTT parameters:

![alt text](img/tasmota-3.png "tasmota-3")

### check all of them

```
/ $ mosquitto_sub -h mqttbrk1.res.training -t tele/# -v
```
you should see something like this:
```
tele/line/2/LWT Online
tele/line/office/ac/LWT Online
tele/line/office/racktr/LWT Online
tele/line/andi/ac/LWT Online
tele/temp/office/ac/LWT Online
...
```

### check the specific one from above

depending on how often/seldom it sends messages you might need to wait

```
/ $ mosquitto_sub -h mqttbrk1.res.training -t tele/temp/office/ac/# -v
```
you should see something like this:
```
tele/temp/office/ac/LWT Online
tele/temp/office/ac/STATE {"Time":"2021-02-25T13:03:01","Uptime":"9T05:01:32","UptimeSec":795692,"Heap":25,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":14,"POWER":"ON","Wifi":{"AP":1,"SSId":"TP-Link_60B7","BSSId":"98:DA:C4:5B:60:B7","Channel":3,"RSSI":86,"Signal":-57,"LinkCount":2,"Downtime":"0T00:00:06"}}
tele/temp/office/ac/SENSOR {"Time":"2021-02-25T13:03:01","SI7021":{"Temperature":22.1,"Humidity":46.4,"DewPoint":10.0},"TempUnit":"C"}
tele/temp/office/ac/STATE {"Time":"2021-02-25T13:08:01","Uptime":"9T05:06:32","UptimeSec":795992,"Heap":25,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":14,"POWER":"ON","Wifi":{"AP":1,"SSId":"TP-Link_60B7","BSSId":"98:DA:C4:5B:60:B7","Channel":3,"RSSI":90,"Signal":-55,"LinkCount":2,"Downtime":"0T00:00:06"}}
tele/temp/office/ac/SENSOR {"Time":"2021-02-25T13:08:01","SI7021":{"Temperature":22.1,"Humidity":46.4,"DewPoint":10.0},"TempUnit":"C"}
tele/temp/office/ac/STATE {"Time":"2021-02-25T13:13:01","Uptime":"9T05:11:32","UptimeSec":796292,"Heap":25,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":14,"POWER":"ON","Wifi":{"AP":1,"SSId":"TP-Link_60B7","BSSId":"98:DA:C4:5B:60:B7","Channel":3,"RSSI":82,"Signal":-59,"LinkCount":2,"Downtime":"0T00:00:06"}}
tele/temp/office/ac/SENSOR {"Time":"2021-02-25T13:13:01","SI7021":{"Temperature":22.1,"Humidity":46.2,"DewPoint":10.0},"TempUnit":"C"}
tele/temp/office/ac/STATE {"Time":"2021-02-25T13:18:01","Uptime":"9T05:16:32","UptimeSec":796592,"Heap":25,"SleepMode":"Dynamic","Sleep":50,"LoadAvg":19,"MqttCount":14,"POWER":"ON","Wifi":{"AP":1,"SSId":"TP-Link_60B7","BSSId":"98:DA:C4:5B:60:B7","Channel":3,"RSSI":72,"Signal":-64,"LinkCount":2,"Downtime":"0T00:00:06"}}
tele/temp/office/ac/SENSOR {"Time":"2021-02-25T13:18:01","SI7021":{"Temperature":22.1,"Humidity":46.1,"DewPoint":10.0},"TempUnit":"C"}
...
```
