#!/bin/sh
set -x
rm -f .env
echo "HERE=$(pwd)" > .env
ln -sf .env env
cat env
set +x
