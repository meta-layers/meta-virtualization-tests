#!/bin/sh

HERE=$(pwd)

if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo
fi

set -x
mkdir -p ${HERE}/projects/tig/data/influxdb/
mkdir -p ${HERE}/projects/tig/data/grafana/
mkdir -p ${HERE}/projects/tig/log/grafana/
mkdir -p ${HERE}/projects/tig/data/mqtt/
mkdir -p ${HERE}/projects/tig/log/mqtt/
cp -r conf ${HERE}/projects/
${SUDO} chmod 777 -R ${HERE}/projects/tig/
set +x
