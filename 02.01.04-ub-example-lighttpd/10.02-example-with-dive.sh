#!/bin/sh
set -x
IMAGE_NAME="reslocal/dive-example-lighttpd"
IMAGE_ID=$(docker images --format="{{.Repository}} {{.ID}}" | grep "^${IMAGE_NAME} " | cut -d' ' -f2)
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest ${IMAGE_ID}
set +x
