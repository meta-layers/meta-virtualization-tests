#!/bin/bash
set -x
pushd local_scripts
./docker_run.sh reslocal/dive-example /opt/usr/local/bin/app.sh
popd
set +x
