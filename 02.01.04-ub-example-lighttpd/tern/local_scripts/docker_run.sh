source ../container-name.sh
IMAGE_NAME=$1

if [ $# -lt 2 ];
then
    echo "+ $0: Too few arguments!"
#   echo "+ interactive:"
#   echo "+  docker run -it reslocal/${CONTAINER_NAME} \"report -i golang:alpine\""
    echo "+ non-interactive:"
    echo "+  $0 reslocal/${CONTAINER_NAME}  \"report -i reslocal/dive-example-lighttpd:latest\""
    echo "it will be output to output.txt"
    exit
fi

# remove currently running containers
echo "+ ID_TO_KILL=\$(docker ps -a -q  --filter ancestor=$1)"
ID_TO_KILL=$(docker ps -a -q  --filter ancestor=$1)

echo "+ docker ps -a"
docker ps -a
echo "+ docker stop ${ID_TO_KILL}"
docker stop ${ID_TO_KILL}
echo "+ docker rm -f ${ID_TO_KILL}"
docker rm -f ${ID_TO_KILL}
echo "+ docker ps -a"
docker ps -a

# -t : Allocate a pseudo-tty
# -i : Keep STDIN open even if not attached
# -d : To start a container in detached mode, you use -d=true or just -d option.
# -p : publish port PUBLIC_PORT:INTERNAL_PORT
# -l : ??? without it no root@1928719827
# --cap-drop=all: drop all (root) capabilites

# Usage: ./docker_run.sh <tern image> <tern command arguments in quotes> > output.txt
# Example: ./docker_run.sh ternd "report -i golang:alpine" > output.txt

docker run --privileged --device /dev/fuse -v /var/run/docker.sock:/var/run/docker.sock --rm ${IMAGE_NAME} $2 > output.txt

echo "+ ID=\$(docker run -t -i $@)"
ID=$(docker run -t -i $@)

echo "+ ID ${ID}"

# let's attach to it:
#echo "+ docker attach ${ID}"
#docker attach ${ID}
