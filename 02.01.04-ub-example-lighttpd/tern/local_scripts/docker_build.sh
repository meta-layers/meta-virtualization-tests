#!/bin/bash
set -x
source ../container-name.sh
# --rm=true: remove intermediate containers after build:
# docker build --rm=true -t reslocal/${CONTAINER_NAME} ../dockerfile/
# don't remove intermediate containers after build:
pushd ~/projects/tern/tern
docker build -t reslocal/${CONTAINER_NAME} docker
popd
set +x
