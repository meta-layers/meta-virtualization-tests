#!/bin/bash
set -x
if [ -d ~/projects/tern ]; then
   rm -rf ~/projects/tern
fi
mkdir -p ~/projects/tern
pushd ~/projects/tern
git clone https://github.com/tern-tools/tern.git
popd
set +x
